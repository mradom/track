<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Orden;
use App\Prestador;

class Garantia extends Model
{
    const SIN_GARANTIA = 1;
    const CON_GARANTIA = 2;
    const CON_GARANTIA_EXTENDIDA = 3;
    const GARANTIA_CASA = 4;

    public $appends = ['nombre'];

	protected $fillable = [
        'tipo', 'fecha', 'lugar','norden','prestador_id'
    ];

    public function getNombreAttribute()
    {
        switch ($this->tipo) {
            case '1':
                return "Sin Garantia";
                break;
            
            case '2':
                return "Con Garantia";
                break;
            case '3':
                return "Con Garantia Extendida";
                break;
            case '4':
                return "Con Garantia de la Casa";
                break;
            default:
                return "Error";
                break;
        }
    }

    public function setFechaAttribute($value)
    {
        $this->attributes['fecha'] = date('Y-m-d', strtotime($value));
    }

    public function orden()
    {
    	return $this->hasOne(Orden::class);
    }

    public function aseguradora()
    {
        return $this->hasOne(Prestador::class,'id', 'prestador_id');
    }

}
