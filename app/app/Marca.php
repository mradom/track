<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Marca extends Model
{
    protected $guarded = ['id'];

    public function tipo()
    {
        return $this->hasOne(Tipo::class,'id', 'tipo_id');
    }

    public function modelos()
    {
    	return $this->hasMany(Modelo::class);
    }
}
