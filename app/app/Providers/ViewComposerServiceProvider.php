<?php

namespace App\Providers;


use App\Http\ViewComposers\UserFormComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot() {
        View::composer('users.partials.form-fields', UserFormComposer::class);

    }
}
