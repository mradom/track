<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{

	public $appends = ['nombrecompleto'];

	protected $fillable = [
        'nombre', 'apellido', 'dni', 'direccion', 'telefono', 'email', 'tipo'
    ];

    public function getNombrecompletoAttribute()
    {
    	return $this->apellido . ", " . $this->nombre;
    }
}
