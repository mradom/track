<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Cliente;

class ClientePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dni'           => 'required|string',
            'apellido'      => 'required|string',
            'nombre'        => 'required|string',
            'tipo'          => 'required',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if ($this->checkIfDniExists()) {
                $validator->errors()->add('dni', '¡Ya existe un cliente con este dni!');
            }
        });
    }

    protected function checkIfDniExists()
    {
        return Cliente::where('dni', $this->input('dni'))
            ->where('id', '!=', $this->input('id'))
            ->first();
    }
}
