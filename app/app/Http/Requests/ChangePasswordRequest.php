<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $can =
            url()->current() == route('users.password.edit', ['user' => logged_user('id')]) ||
            url()->current() == route('users.password.update', ['user' => logged_user('id')]);

        return $can;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->isMethod('POST') ? $this->getPostRules() : [];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($this->isMethod('POST')) {
            $validator->after(function ($validator) {
                if (!logged_user()->check($this->input('current'))) {
                    $validator->errors()->add('current', 'La contraseña actual es incorrecta.');
                }
            });
        }
    }

    protected function getPostRules() : array
    {
        return [
            'current' => 'required|string',
            'new'     => 'required|string|min:6',
            'repeat'  => 'required|string|same:new'
        ];
    }
}
