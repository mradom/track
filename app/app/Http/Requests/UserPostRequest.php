<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;

class UserPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'required|string',
            'email' => 'required|email',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if ($this->checkIfEmailExists()) {
                $validator->errors()->add('email', '¡Ya existe un usuario con ese correo electrónico!');
            }
        });
    }

    protected function checkIfEmailExists()
    {
        return User::where('email', $this->input('email'))
            ->where('id', '!=', $this->input('id'))
            ->first();
    }
}
