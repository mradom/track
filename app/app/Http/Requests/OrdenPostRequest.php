<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrdenPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cliente_id'    => 'required|numeric',
            'tipo_id'     => 'required|numeric',
            'marca_id'     => 'required|numeric',
            'modelo_id'     => 'required|numeric',
            'color'         => 'required|string',
            'esn'           => 'required|string',
            'falla'         => 'required|string',
            'accesorios'    => 'required|string',
            'garantia'      => 'required',
            'prestador'      => 'required',
            'clave'      => 'required',
            'estado'      => 'required',
        ];
    }
}
