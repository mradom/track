<?php

namespace App\Http\ViewComposers;

use App\User;
use Illuminate\View\View;

class UserFormComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view) {
        $systemUser = $view->system_user ?: (new User);

        if (! $systemUser->id) {
            $systemUser->fill([
                'name' => old('name')
            ]);
        }

        $view->with('system_user', $systemUser);
    }
}
