<?php

namespace App\Http\Controllers;

use App\Orden;
use Illuminate\Http\Request;
use App\Cliente;
use App\Tipo;
use App\Marca;
use App\Http\Requests\OrdenPostRequest;
use Illuminate\Support\Facades\DB;
use App\Garantia;
use App\Estado;
use App\Historia;
use Auth;
use App\Exports\OrdenExport;
use PDF;
use App\Prestador;
use DataTables;

class OrdenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('orden.index');
    }

    public function load()
    {
        $datatables = datatables()->of(Orden::query()->with(['clienteRel','modeloRel','estadoRel'])->orderBy('id', 'desc'));

        $datatables->addColumn('cliente', function($row){
                    return $row->clienteRel->nombrecompleto;
                });

        return $datatables->make(true);

        /*
                ->addColumn('clientenombre', function($row){
                    return $row->clienteRel->nombrecompleto;
                })
                )
                ->toJson();

        /*$model = Orden::with(['clienteRel','modeloRel','estado']);
        return DataTables::eloquent($model)
                ->addColumn('clienteRel', function (Orden $orden) {
                    return $orden->clienteRel->map(function($clienteRel) {
                        return str_limit($clienteRel->apellido, 30, '...');
                    })->implode('<br>');
                })
                ->toJson();*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Cliente $cliente)
    {
        $data = [
            "cliente"   => $cliente,
            'tipos' => Tipo::all()->sortBy('nombre'),
            'marcas' => Marca::all()->sortBy('nombre'),
            'prestadores'   => Prestador::all()->sortBy('nombre'),
        ];
        return view("orden.create", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrdenPostRequest $request)
    {
        try {
            DB::beginTransaction();
                $garantia = Garantia::create([
                    'tipo'  => $request->input('garantia'),
                    'fecha'  => $request->input('fecha'),
                    'lugar'  => $request->input('lugar'),
                    'norden'  => $request->input('norden'),
                    'prestador_id'  => $request->input('aseguradora'),
                ]);

                if ($request->input('diagnostico') != "" and $request->input('precio') != "" )
                {
                    $estado_id = 5;
                }else{
                    $estado_id = Estado::all()->sortBy('orden')->first()->id;
                }

                $orden = Orden::create([
                    'cliente_id'    => $request->input('cliente_id'),
                    'modelo_id'     => $request->input('modelo_id'),
                    'color'         => $request->input('color'),
                    'esn'           => $request->input('esn'),
                    'falla'         => $request->input('falla'),
                    'accesorios'    => $request->input('accesorios'),
                    'prestador'     => $request->input('prestador'),
                    'clave'         => $request->input('clave'),
                    'precio'        => $request->input('precio'),
                    'diagnostico'   => $request->input('diagnostico'),
                    'estado'        => $request->input('estado'),
                    'garantia_id'   => $garantia->id,
                    'estado_id'     => $estado_id,
                ]);

                $historia = Historia::create([
                    'accion'    => 'Nuevo',
                    'orden_id'  => $orden->id,
                    'user_id'   => Auth::user()->id,
                    'mensaje'   => 'Creacion de nueva orden',
                ]);

                if ($request->input('diagnostico') != "" and $request->input('precio') != "" ) {
                    $historia = Historia::create([
                        'accion'    => 'Presupuesto Aprobado',
                        'orden_id'  => $orden->id,
                        'user_id'   => Auth::user()->id,
                        'mensaje'   => 'Orden aprobada por el cliente en el local',
                    ]);
                }

                if ($request->input('otro') != "") {
                    $historia = Historia::create([
                        'accion'    => 'Nuevo',
                        'orden_id'  => $orden->id,
                        'user_id'   => Auth::user()->id,
                        'mensaje'   => 'Equipo no registrado: ' . $request->input('otro'),
                    ]);
                }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
        }

        return redirect(route('orden.show', ['orden'=>$orden->id]))->with('flash_message', [
            'type' => 'success',
            'message' => 'Orden '.$orden->id.' creada exitosamente.'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Orden  $orden
     * @return \Illuminate\Http\Response
     */
    public function show(Orden $orden)
    {
        
        return view('orden.show', ['orden'=>$orden, 'estados' => Estado::all()]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Orden  $orden
     * @return \Illuminate\Http\Response
     */
    public function edit(Orden $orden)
    {
        return view('orden.edit', ['orden'=>$orden]);
    }

    public function update(Request $request, Orden $orden)
    {

        $orden->esn = $request->input('esn');
        $orden->color = $request->input('color');
        $orden->falla = $request->input('falla');
        $orden->accesorios = $request->input('accesorios');
        $orden->save();
        return redirect(route('orden.show', ['orden'=>$orden->id]))->with('flash_message', [
            'type' => 'success',
            'message' => 'Orden '.$orden->id.' actualizada exitosamente.'
        ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Orden  $orden
     * @return \Illuminate\Http\Response
     */
    public function precio(Request $request, Orden $orden)
    {
        $orden->update(['precio' =>$request->input('precio')]);

        $historia = Historia::create([
            'accion'    => 'Precio',
            'orden_id'  => $orden->id,
            'user_id'   => Auth::user()->id,
            'mensaje'   => 'Nuevo precio de $'.$request->input('precio'),
        ]);

        return redirect(route('orden.show',['orden'=>$orden->id]))->with('flash_message', [
            'type' => 'success',
            'message' => 'Precio actualizado exitosamente.'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Orden  $orden
     * @return \Illuminate\Http\Response
     */
    public function estadoUpdate(Request $request, Orden $orden)
    {
        if ($orden->estado_id < 8) {
            if ($request->input('estado') == 3) {
                $orden->update([
                    'estado_id'     =>$request->input('estado'),
                    'precio'        =>$request->input('precio'),
                    'diagnostico'   => $request->input('diagnostico'),
                ]);

                $historia = Historia::create([
                    'accion'    => 'Precio',
                    'orden_id'  => $orden->id,
                    'user_id'   => Auth::user()->id,
                    'mensaje'   => 'Nuevo precio de $'.$request->input('precio'),
                ]);
            }else{
                $orden->update(['estado_id' => $request->input('estado')]);
            }

            $historia = Historia::create([
                'accion'    => 'Estado',
                'orden_id'  => $orden->id,
                'user_id'   => Auth::user()->id,
                'mensaje'   => 'Cambio de estado a '. $orden->estadoRel->nombre,
            ]);
            return redirect(route('orden.show',['orden'=>$orden->id]))->with('flash_message', [
                'type' => 'success',
                'message' => 'Cambio de estado a ' . $orden->estadoRel->nombre . " exitoso."
            ]);
        }else{
            return redirect(route('orden.show',['orden'=>$orden->id]))->with('flash_message', [
                'type' => 'info',
                'message' => 'No es posible cambiar el estado de la orden, porque ya ha sido cerrada'
        ]);
        }
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Orden  $orden
     * @return \Illuminate\Http\Response
     */
    public function destroy(Orden $orden)
    {
        //
    }

    /**
     * Print the specified resource from storage.
     *
     * @param  \App\Orden  $orden
     * @return \Illuminate\Http\Response
     */
    public function print(Orden $orden)
    {
        $data['orden'] = $orden;
        $data['cliente'] = $orden->clienteRel;
        $data['equipo'] = $orden->modeloRel;
        $data['garantia'] = $orden->garantia;
        $pdf = PDF::loadView('orden.ticket', $data);
        return $pdf->stream();
    }
}
