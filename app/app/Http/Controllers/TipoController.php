<?php

namespace App\Http\Controllers;

use App\Tipo;
use Illuminate\Http\Request;
use App\Http\Requests\TipoPostRequest;
use App\Marca;
use App\Modelo;

class TipoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("tipo.index", ['tipos' => Tipo::all()->sortBy('nombre')]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("tipo.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TipoPostRequest $request)
    {
        $tipo = Tipo::create($request->only('nombre'));

        return redirect(route('tipos.index'))->with('flash_message', [
            'type' => 'success',
            'message' => 'Tipo '.$tipo->nombre.' creado exitosamente.'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tipo  $tipo
     * @return \Illuminate\Http\Response
     */
    public function show(Tipo $tipo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tipo  $tipo
     * @return \Illuminate\Http\Response
     */
    public function edit(Tipo $tipo)
    {
        return view("tipo.edit", ['tipo' => $tipo]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tipo  $tipo
     * @return \Illuminate\Http\Response
     */
    public function update(TipoPostRequest $request, Tipo $tipo)
    {
        $tipo->update($request->only('nombre'));

        return redirect(route('tipos.index'))->with('flash_message', [
            'type' => 'success',
            'message' => 'Tipo '.$tipo->nombre.' actualizado exitosamente.'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tipo  $tipo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tipo $tipo)
    {
        //
    }

    public function getMarcas(Tipo $tipo)
    {
        return response()->json(Marca::all()->where('tipo_id', $tipo->id));
    }

    public function getModelos(Tipo $tipo, Marca $marca)
    {
        return response()->json($tipo->marcas->where('id',$marca->id)->first()->modelos);
    }
}
