<?php

namespace App\Http\Controllers;

use App\Marca;
use Illuminate\Http\Request;
use App\Http\Requests\MarcaPostRequest;
use App\Tipo;

class MarcaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("marca.index", ['marcas' => Marca::all()->sortBy('nombre')]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'marcas' => Marca::all()->sortBy('nombre'),
            'tipos' => Tipo::all()->sortBy('nombre'),
        ];
        return view("marca.create", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MarcaPostRequest $request)
    {
        $marca = Marca::create($request->only('nombre', 'tipo_id'));

        return redirect(route('marcas.index'))->with('flash_message', [
            'type' => 'success',
            'message' => 'Marca '.$marca->nombre.' creada exitosamente.'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Marca  $marca
     * @return \Illuminate\Http\Response
     */
    public function show(Marca $marca)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Marca  $marca
     * @return \Illuminate\Http\Response
     */
    public function edit(Marca $marca)
    {
        $data = [
            'marca' => $marca,
            'tipos' => Tipo::all()->sortBy('nombre'),
        ];
        return view("marca.edit",$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Marca  $marca
     * @return \Illuminate\Http\Response
     */
    public function update(MarcaPostRequest $request, Marca $marca)
    {
        $marca->update($request->only('nombre', 'tipo_id'));

        return redirect(route('marcas.index'))->with('flash_message', [
            'type' => 'success',
            'message' => 'Marca '.$marca->nombre.' actualizada exitosamente.'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Marca  $marca
     * @return \Illuminate\Http\Response
     */
    public function destroy(Marca $marca)
    {
        //
    }
}
