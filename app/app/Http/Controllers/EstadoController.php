<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Estado;

class EstadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('estado.index', ['estados' => Estado::all()->sortBy('orden')]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('estado.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $orden = Estado::max('orden') + 1;
        $estado = Estado::create(['nombre' => $request->input('nombre'), 'orden' => $orden]);

        return redirect(route('estados.index'))->with('flash_message', [
            'type' => 'success',
            'message' => 'Estado '.$estado->nombre.' creada exitosamente.'
        ]);
    }

    public function up(Estado $estado)
    {
        Estado::where('orden',$estado->orden - 1)->update(['orden' => $estado->orden]);
        $estado->update(['orden' => $estado->orden-1]);

        return redirect(route('estados.index'))->with('flash_message', [
            'type' => 'success',
            'message' => 'Cambio de orden de '.$estado->nombre.' exitosamente.'
        ]);
    }

    public function down(Estado $estado)
    {
        Estado::where('orden',$estado->orden + 1)->update(['orden' => $estado->orden]);
        $estado->update(['orden' => $estado->orden+1]);

        return redirect(route('estados.index'))->with('flash_message', [
            'type' => 'success',
            'message' => 'Cambio de orden de '.$estado->nombre.' exitosamente.'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
