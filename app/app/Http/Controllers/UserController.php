<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\UserPostRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('users.index', ['users' => User::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Request\UserPostRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserPostRequest $request)
    {
        $user = User::create([
            'name'      => $request->input('name'),
            'email'     => $request->input('email'),
            'password'  => 'allmobile2019'
        ]);

        return redirect(route('users.index'))->with('flash_message', [
            'type' => 'success',
            'message' => 'Usuario '.$user->name.' creado exitosamente.'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return redirect()->route('users.edit', ['system_user' => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('users.edit', ['system_user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Request\UserPostRequest  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserPostRequest $request, User $user)
    {
        $user->update($request->only('name', 'email'));

        return redirect(route('users.index'))->with('flash_message', [
            'type' => 'success',
            'message' => 'Usuario '.$user->name.' actualizado exitosamente.'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        Session::flash('flash_message', [
            'type' => 'success',
            'message' => 'Usuario '.$user->name.' eliminado exitosamente.'
        ]);
    }

    /**
     * Show change password form
     *
     * @param  \App\Http\Request\UserPostRequest  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function editPassword(ChangePasswordRequest $request, User $user)
    {
        return view('users.edit_password', ['system_user' => $user]);
    }

    /**
     * Update current user password
     *
     * @param  \App\Http\Request\UserPostRequest  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function updatePassword(ChangePasswordRequest $request, User $user)
    {
        $user->update([
            'password' => $request->input('new')
        ]);

        return redirect(route('users.password.edit', ['user' => $user]))->with('flash_message', [
            'type' => 'success',
            'message' => 'Contraseña actualizada exitosamente.'
        ]);
    }
}
