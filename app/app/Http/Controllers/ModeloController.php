<?php

namespace App\Http\Controllers;

use App\Modelo;
use Illuminate\Http\Request;
use App\Marca;
use App\Tipo;
use App\Http\Requests\ModeloPostRequest;

class ModeloController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("modelo.index", ["modelos" => Modelo::all()->sortBy('nombre')]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'tipos' => Tipo::all()->sortBy('nombre'),
            'marcas' => Marca::all()->sortBy('nombre'),
        ];
        return view("modelo.create", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ModeloPostRequest $request)
    {
        $modelo = Modelo::create($request->only('nombre', 'marca_id'));

        return redirect(route('modelos.index'))->with('flash_message', [
            'type' => 'success',
            'message' => 'Modelo '.$modelo->nombre.' creado exitosamente.'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Modelo  $modelo
     * @return \Illuminate\Http\Response
     */
    public function show(Modelo $modelo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Modelo  $modelo
     * @return \Illuminate\Http\Response
     */
    public function edit(Modelo $modelo)
    {
        $data = [
            'modelo' => $modelo,
            'marcas' => Marca::all()->sortBy('nombre'),
        ];
        return view("modelo.edit",$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Modelo  $modelo
     * @return \Illuminate\Http\Response
     */
    public function update(ModeloPostRequest $request, Modelo $modelo)
    {
        $modelo->update($request->only('nombre', 'marca_id'));

        return redirect(route('modelos.index'))->with('flash_message', [
            'type' => 'success',
            'message' => 'Modelo '.$modelo->nombre.' actualizado exitosamente.'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Modelo  $modelo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Modelo $modelo)
    {
        //
    }
}
