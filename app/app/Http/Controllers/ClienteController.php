<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;
use App\Http\Requests\ClientePostRequest;
use Illuminate\Support\Facades\DB;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cliente.index');
    }

    public function load()
    {
        return datatables()->of(Cliente::query()->orderBy('id', 'desc'))->toJson();
    }

    public function create()
    {
		return view('cliente.create');
    }

    public function buscar($dni)
    {
        return response()->json(Cliente::all()->where('dni', $dni)->first());
    }

    public function view(Cliente $cliente)
    {
        return view('cliente.show', ["cliente" => $cliente]);
    }

    public function edit(Cliente $cliente)
    {
        return view('cliente.edit', ["cliente" => $cliente]);
    }

    public function update(Request $request, Cliente $cliente)
    {

            $cliente->update($request->only('apellido','nombre','direccion','telefono','email'));

            return redirect(route('cliente.index'))->with('flash_message', [
                'type' => 'success',
                'message' => 'Cliente '.$cliente->id.' actualizado exitosamente.'
            ]);
    }

    public function store(ClientePostRequest $request)
    {

        if ($request->input('tipo') == "dni") {
            $tipo = 1;
        }else{
            $tipoe = 2;
        }

        try {
            DB::beginTransaction();
                $cliente = Cliente::create([
                    'dni'       => $request->input('dni'),
                    'apellido'  => $request->input('apellido'),
                    'nombre'    => $request->input('nombre'),
                    'direccion' => $request->input('direccion'),
                    'telefono'  => $request->input('telefono'),
                    'email'     => $request->input('email'),
                    'tipo'      => $tipo,
                ]);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
        }

        return $cliente->toJson();
    }
}
