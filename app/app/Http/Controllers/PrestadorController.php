<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Prestador;
use App\Http\Requests\PrestadorPostRequest;

class PrestadorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("prestador.index", ['prestadores' => Prestador::all()->sortBy('nombre')]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("prestador.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PrestadorPostRequest $request)
    {
        $prestador = Prestador::create($request->only('nombre'));

        return redirect(route('prestadores.index'))->with('flash_message', [
            'type' => 'success',
            'message' => 'Prestador '.$prestador->nombre.' creado exitosamente.'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Prestador $prestadore)
    {
        $data = [
            'prestador' => $prestadore,
        ];
        return view("prestador.edit", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PrestadorPostRequest $request, Prestador $prestadore)
    {
        $prestadore->update($request->only('nombre'));

        return redirect(route('prestadores.index'))->with('flash_message', [
            'type' => 'success',
            'message' => 'Prestador '.$prestadore->nombre.' actualizado exitosamente.'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Prestador $prestadore)
    {
        $prestadore->delete();

        return redirect(route('prestadores.index'))->with('flash_message', [
            'type' => 'success',
            'message' => 'Prestador '.$prestadore->nombre.' eliminado exitosamente.'
        ]);
    }
}
