<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Orden;

class Historia extends Model
{
    protected $guarded = ['id'];

    protected $casts = [
		'created_at' => 'datetime:d/m/Y',
	];

    public function orden()
    {
    	return $this->belongsTo(Orden::class);
    }

    public function usuario()
    {
    	return $this->belongsTo(User::class,'user_id');
    }
}
