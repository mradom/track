<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modelo extends Model
{
    protected $guarded = ['id'];
    public $appends = ['nombrecompleto'];

    public function marca()
    {
        return $this->hasOne(Marca::class,'id', 'marca_id')->with('tipo');
    }

    public function getNombrecompletoAttribute()
    {
    	return $this->marca->nombre . " - " . $this->nombre;
    }

}
