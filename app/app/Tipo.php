<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipo extends Model
{
    protected $guarded = ['id'];

    public function marcas()
    {
        return $this->hasMany(Marca::class)->with('modelos');
    }
}
