<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
	protected $fillable = [
        'nombre', 'orden'
    ];

    public function getNextOrden()
    {
    	return $this->max('orden') + 1;
    }
}
