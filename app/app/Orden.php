<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable;
use App\Historia;


class Orden extends Model implements Auditable
{
    use AuditableTrait;

	protected $table = 'ordenes';
    protected $guarded = ['id'];
    protected $appendd = ['clienteNombre'];

    public function clienteRel()
    {
        return $this->hasOne(Cliente::class,'id','cliente_id');
    }

    public function modeloRel()
    {
    	return $this->hasOne(Modelo::class,'id','modelo_id')->with('marca');
    }

    public function garantia()
    {
        return $this->hasOne(Garantia::class, 'id', 'garantia_id')->with('aseguradora');
    }

    public function getClienteNombreAttribute()
    {
    	return $this->clienteRel->nombrecompleto;
    }

    public function estadoRel()
    {
        return $this->hasOne(Estado::class,'id','estado_id');
    }

    public function historias()
    {
        return $this->hasMany(Historia::class);
    }

    public function fotos()
    {
        return $this->hasMany(Fotos::class);
    }
}