<?php

namespace App\Exports;

use App\Orden;
use Maatwebsite\Excel\Concerns\FromCollection;

class OrdenExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Orden::all();
    }
}
