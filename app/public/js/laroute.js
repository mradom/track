(function () {

    var laroute = (function () {

        var routes = {

            absolute: false,
            rootUrl: 'http://localhost',
            routes : [{"host":null,"methods":["GET","HEAD"],"uri":"login","name":"login","action":"App\Http\Controllers\Auth\LoginController@showLoginForm"},{"host":null,"methods":["POST"],"uri":"login","name":null,"action":"App\Http\Controllers\Auth\LoginController@login"},{"host":null,"methods":["POST"],"uri":"logout","name":"logout","action":"App\Http\Controllers\Auth\LoginController@logout"},{"host":null,"methods":["GET","HEAD"],"uri":"register","name":"register","action":"App\Http\Controllers\Auth\RegisterController@showRegistrationForm"},{"host":null,"methods":["POST"],"uri":"register","name":null,"action":"App\Http\Controllers\Auth\RegisterController@register"},{"host":null,"methods":["GET","HEAD"],"uri":"password\/reset","name":"password.request","action":"App\Http\Controllers\Auth\ForgotPasswordController@showLinkRequestForm"},{"host":null,"methods":["POST"],"uri":"password\/email","name":"password.email","action":"App\Http\Controllers\Auth\ForgotPasswordController@sendResetLinkEmail"},{"host":null,"methods":["GET","HEAD"],"uri":"password\/reset\/{token}","name":"password.reset","action":"App\Http\Controllers\Auth\ResetPasswordController@showResetForm"},{"host":null,"methods":["POST"],"uri":"password\/reset","name":null,"action":"App\Http\Controllers\Auth\ResetPasswordController@reset"},{"host":null,"methods":["GET","HEAD"],"uri":"home","name":null,"action":"App\Http\Controllers\HomeController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"_dusk\/login\/{userId}\/{guard?}","name":null,"action":"Laravel\Dusk\Http\Controllers\UserController@login"},{"host":null,"methods":["GET","HEAD"],"uri":"_dusk\/logout\/{guard?}","name":null,"action":"Laravel\Dusk\Http\Controllers\UserController@logout"},{"host":null,"methods":["GET","HEAD"],"uri":"_dusk\/user\/{guard?}","name":null,"action":"Laravel\Dusk\Http\Controllers\UserController@user"},{"host":null,"methods":["GET","HEAD"],"uri":"api\/user","name":null,"action":"Closure"},{"host":null,"methods":["GET","HEAD"],"uri":"\/","name":null,"action":"Closure"},{"host":null,"methods":["GET","HEAD"],"uri":"orden\/load","name":"orden.load","action":"App\Http\Controllers\OrdenController@load"},{"host":null,"methods":["GET","HEAD"],"uri":"cliente","name":"cliente.index","action":"App\Http\Controllers\ClienteController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"cliente\/load","name":"cliente.load","action":"App\Http\Controllers\ClienteController@load"},{"host":null,"methods":["GET","HEAD"],"uri":"cliente\/crear","name":"cliente.crear","action":"App\Http\Controllers\ClienteController@create"},{"host":null,"methods":["POST"],"uri":"cliente\/crear","name":"cliente.store","action":"App\Http\Controllers\ClienteController@store"},{"host":null,"methods":["POST"],"uri":"cliente\/{cliente}","name":"cliente.view","action":"App\Http\Controllers\ClienteController@view"},{"host":null,"methods":["GET","HEAD"],"uri":"json\/cliente\/buscar\/{dni}","name":"cliente.buscar","action":"App\Http\Controllers\ClienteController@buscar"},{"host":null,"methods":["GET","HEAD"],"uri":"json\/tipo\/{tipo}\/marcas","name":"marcas.tipo","action":"App\Http\Controllers\TipoController@getMarcas"},{"host":null,"methods":["GET","HEAD"],"uri":"json\/tipo\/{tipo}\/marca\/{marca}\/modelos","name":"tipos.marcas.modelos","action":"App\Http\Controllers\TipoController@getModelos"},{"host":null,"methods":["GET","HEAD"],"uri":"tipos","name":"tipos.index","action":"App\Http\Controllers\TipoController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"tipos\/crear","name":"tipos.create","action":"App\Http\Controllers\TipoController@create"},{"host":null,"methods":["POST"],"uri":"tipos","name":"tipos.store","action":"App\Http\Controllers\TipoController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"tipos\/{tipo}","name":"tipos.show","action":"App\Http\Controllers\TipoController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"tipos\/{tipo}\/editar","name":"tipos.edit","action":"App\Http\Controllers\TipoController@edit"},{"host":null,"methods":["PUT","PATCH"],"uri":"tipos\/{tipo}","name":"tipos.update","action":"App\Http\Controllers\TipoController@update"},{"host":null,"methods":["DELETE"],"uri":"tipos\/{tipo}","name":"tipos.destroy","action":"App\Http\Controllers\TipoController@destroy"},{"host":null,"methods":["GET","HEAD"],"uri":"marcas","name":"marcas.index","action":"App\Http\Controllers\MarcaController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"marcas\/crear","name":"marcas.create","action":"App\Http\Controllers\MarcaController@create"},{"host":null,"methods":["POST"],"uri":"marcas","name":"marcas.store","action":"App\Http\Controllers\MarcaController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"marcas\/{marca}","name":"marcas.show","action":"App\Http\Controllers\MarcaController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"marcas\/{marca}\/editar","name":"marcas.edit","action":"App\Http\Controllers\MarcaController@edit"},{"host":null,"methods":["PUT","PATCH"],"uri":"marcas\/{marca}","name":"marcas.update","action":"App\Http\Controllers\MarcaController@update"},{"host":null,"methods":["DELETE"],"uri":"marcas\/{marca}","name":"marcas.destroy","action":"App\Http\Controllers\MarcaController@destroy"},{"host":null,"methods":["GET","HEAD"],"uri":"modelos","name":"modelos.index","action":"App\Http\Controllers\ModeloController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"modelos\/crear","name":"modelos.create","action":"App\Http\Controllers\ModeloController@create"},{"host":null,"methods":["POST"],"uri":"modelos","name":"modelos.store","action":"App\Http\Controllers\ModeloController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"modelos\/{modelo}","name":"modelos.show","action":"App\Http\Controllers\ModeloController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"modelos\/{modelo}\/editar","name":"modelos.edit","action":"App\Http\Controllers\ModeloController@edit"},{"host":null,"methods":["PUT","PATCH"],"uri":"modelos\/{modelo}","name":"modelos.update","action":"App\Http\Controllers\ModeloController@update"},{"host":null,"methods":["DELETE"],"uri":"modelos\/{modelo}","name":"modelos.destroy","action":"App\Http\Controllers\ModeloController@destroy"},{"host":null,"methods":["GET","HEAD"],"uri":"estados\/{estado}\/arriba","name":"estado.up","action":"App\Http\Controllers\EstadoController@up"},{"host":null,"methods":["GET","HEAD"],"uri":"estados\/{estado}\/abajo","name":"estado.down","action":"App\Http\Controllers\EstadoController@down"},{"host":null,"methods":["GET","HEAD"],"uri":"estados","name":"estados.index","action":"App\Http\Controllers\EstadoController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"estados\/crear","name":"estados.create","action":"App\Http\Controllers\EstadoController@create"},{"host":null,"methods":["POST"],"uri":"estados","name":"estados.store","action":"App\Http\Controllers\EstadoController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"estados\/{estado}","name":"estados.show","action":"App\Http\Controllers\EstadoController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"estados\/{estado}\/editar","name":"estados.edit","action":"App\Http\Controllers\EstadoController@edit"},{"host":null,"methods":["PUT","PATCH"],"uri":"estados\/{estado}","name":"estados.update","action":"App\Http\Controllers\EstadoController@update"},{"host":null,"methods":["DELETE"],"uri":"estados\/{estado}","name":"estados.destroy","action":"App\Http\Controllers\EstadoController@destroy"},{"host":null,"methods":["GET","HEAD"],"uri":"orden","name":"orden.index","action":"App\Http\Controllers\OrdenController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"orden\/{orden}","name":"orden.show","action":"App\Http\Controllers\OrdenController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"orden\/crear\/cliente\/{cliente}","name":"orden.create","action":"App\Http\Controllers\OrdenController@create"},{"host":null,"methods":["POST"],"uri":"orden\/crear","name":"orden.store","action":"App\Http\Controllers\OrdenController@store"},{"host":null,"methods":["POST"],"uri":"orden\/{orden}\/precio","name":"orden.precio","action":"App\Http\Controllers\OrdenController@precio"},{"host":null,"methods":["POST"],"uri":"orden\/{orden}\/estado","name":"orden.estadoupdate","action":"App\Http\Controllers\OrdenController@estadoUpdate"},{"host":null,"methods":["GET","HEAD"],"uri":"orden\/{orden}\/print","name":"orden.print","action":"App\Http\Controllers\OrdenController@print"},{"host":null,"methods":["GET","HEAD"],"uri":"usuarios","name":"users.index","action":"App\Http\Controllers\UserController@index"},{"host":null,"methods":["GET","HEAD"],"uri":"usuarios\/crear","name":"users.create","action":"App\Http\Controllers\UserController@create"},{"host":null,"methods":["POST"],"uri":"usuarios","name":"users.store","action":"App\Http\Controllers\UserController@store"},{"host":null,"methods":["GET","HEAD"],"uri":"usuarios\/{user}","name":"users.show","action":"App\Http\Controllers\UserController@show"},{"host":null,"methods":["GET","HEAD"],"uri":"usuarios\/{user}\/editar","name":"users.edit","action":"App\Http\Controllers\UserController@edit"},{"host":null,"methods":["POST"],"uri":"usuarios\/{user}","name":"users.update","action":"App\Http\Controllers\UserController@update"},{"host":null,"methods":["DELETE"],"uri":"usuarios\/{user}","name":"users.destroy","action":"App\Http\Controllers\UserController@destroy"},{"host":null,"methods":["GET","HEAD"],"uri":"acceso\/{user}\/cambiar","name":"users.password.edit","action":"App\Http\Controllers\UserController@editPassword"},{"host":null,"methods":["POST"],"uri":"acceso\/{user}","name":"users.password.update","action":"App\Http\Controllers\UserController@updatePassword"},{"host":null,"methods":["GET","HEAD"],"uri":"historial\/{orden}\/load","name":"historia.load","action":"App\Http\Controllers\HistoriaController@load"}],
            prefix: '',

            route : function (name, parameters, route) {
                route = route || this.getByName(name);

                if ( ! route ) {
                    return undefined;
                }

                return this.toRoute(route, parameters);
            },

            url: function (url, parameters) {
                parameters = parameters || [];

                var uri = url + '/' + parameters.join('/');

                return this.getCorrectUrl(uri);
            },

            toRoute : function (route, parameters) {
                var uri = this.replaceNamedParameters(route.uri, parameters);
                var qs  = this.getRouteQueryString(parameters);

                if (this.absolute && this.isOtherHost(route)){
                    return "//" + route.host + "/" + uri + qs;
                }

                return this.getCorrectUrl(uri + qs);
            },

            isOtherHost: function (route){
                return route.host && route.host != window.location.hostname;
            },

            replaceNamedParameters : function (uri, parameters) {
                uri = uri.replace(/\{(.*?)\??\}/g, function(match, key) {
                    if (parameters.hasOwnProperty(key)) {
                        var value = parameters[key];
                        delete parameters[key];
                        return value;
                    } else {
                        return match;
                    }
                });

                // Strip out any optional parameters that were not given
                uri = uri.replace(/\/\{.*?\?\}/g, '');

                return uri;
            },

            getRouteQueryString : function (parameters) {
                var qs = [];
                for (var key in parameters) {
                    if (parameters.hasOwnProperty(key)) {
                        qs.push(key + '=' + parameters[key]);
                    }
                }

                if (qs.length < 1) {
                    return '';
                }

                return '?' + qs.join('&');
            },

            getByName : function (name) {
                for (var key in this.routes) {
                    if (this.routes.hasOwnProperty(key) && this.routes[key].name === name) {
                        return this.routes[key];
                    }
                }
            },

            getByAction : function(action) {
                for (var key in this.routes) {
                    if (this.routes.hasOwnProperty(key) && this.routes[key].action === action) {
                        return this.routes[key];
                    }
                }
            },

            getCorrectUrl: function (uri) {
                var url = this.prefix + '/' + uri.replace(/^\/?/, '');

                if ( ! this.absolute) {
                    return url;
                }

                return this.rootUrl.replace('/\/?$/', '') + url;
            }
        };

        var getLinkAttributes = function(attributes) {
            if ( ! attributes) {
                return '';
            }

            var attrs = [];
            for (var key in attributes) {
                if (attributes.hasOwnProperty(key)) {
                    attrs.push(key + '="' + attributes[key] + '"');
                }
            }

            return attrs.join(' ');
        };

        var getHtmlLink = function (url, title, attributes) {
            title      = title || url;
            attributes = getLinkAttributes(attributes);

            return '<a href="' + url + '" ' + attributes + '>' + title + '</a>';
        };

        return {
            // Generate a url for a given controller action.
            // laroute.action('HomeController@getIndex', [params = {}])
            action : function (name, parameters) {
                parameters = parameters || {};

                return routes.route(name, parameters, routes.getByAction(name));
            },

            // Generate a url for a given named route.
            // laroute.route('routeName', [params = {}])
            route : function (route, parameters) {
                parameters = parameters || {};

                return routes.route(route, parameters);
            },

            // Generate a fully qualified URL to the given path.
            // laroute.route('url', [params = {}])
            url : function (route, parameters) {
                parameters = parameters || {};

                return routes.url(route, parameters);
            },

            // Generate a html link to the given url.
            // laroute.link_to('foo/bar', [title = url], [attributes = {}])
            link_to : function (url, title, attributes) {
                url = this.url(url);

                return getHtmlLink(url, title, attributes);
            },

            // Generate a html link to the given route.
            // laroute.link_to_route('route.name', [title=url], [parameters = {}], [attributes = {}])
            link_to_route : function (route, title, parameters, attributes) {
                var url = this.route(route, parameters);

                return getHtmlLink(url, title, attributes);
            },

            // Generate a html link to the given controller action.
            // laroute.link_to_action('HomeController@getIndex', [title=url], [parameters = {}], [attributes = {}])
            link_to_action : function(action, title, parameters, attributes) {
                var url = this.action(action, parameters);

                return getHtmlLink(url, title, attributes);
            }

        };

    }).call(this);

    /**
     * Expose the class either via AMD, CommonJS or the global object
     */
    if (typeof define === 'function' && define.amd) {
        define(function () {
            return laroute;
        });
    }
    else if (typeof module === 'object' && module.exports){
        module.exports = laroute;
    }
    else {
        window.laroute = laroute;
    }

}).call(this);

