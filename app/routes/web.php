<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //return view('welcome');
    return redirect('login');
});

Route::get('/orden/load', 'OrdenController@load')->name('orden.load');

Route::group(['middleware' => 'auth'], function () {

    Route::group(['prefix' => '/cliente'], function() {
        Route::get('/', 'ClienteController@index')->name('cliente.index');
        Route::get('/load', 'ClienteController@load')->name('cliente.load');
        Route::get('/crear', 'ClienteController@create')->name('cliente.crear');
        Route::post('/crear', 'ClienteController@store')->name('cliente.store');
        Route::get('/{cliente}', 'ClienteController@view')->name('cliente.view');
        Route::get('/{cliente}/edit', 'ClienteController@edit')->name('cliente.edit');
        Route::post('/{cliente}/update', 'ClienteController@update')->name('cliente.update');
    });

    Route::group(['prefix' => '/json'], function() {
        Route::get('/cliente/buscar/{dni}', 'ClienteController@buscar')->name('cliente.buscar');
        Route::get('/tipo/{tipo}/marcas', 'TipoController@getMarcas')->name('marcas.tipo');
        Route::get('/tipo/{tipo}/marca/{marca}/modelos', 'TipoController@getModelos')->name('tipos.marcas.modelos');
    });

    Route::resource('tipos', 'TipoController');
    Route::resource('marcas', 'MarcaController');
    Route::resource('modelos', 'ModeloController');
    Route::resource('prestadores', 'PrestadorController');

    Route::resource('fotos', 'FotoController');

    Route::get('/estados/{estado}/arriba', 'EstadoController@up')->name('estado.up');
    Route::get('/estados/{estado}/abajo', 'EstadoController@down')->name('estado.down');

    Route::resource('estados', 'EstadoController');

    Route::group(['prefix' => '/orden'], function() {
        Route::get('/', 'OrdenController@index')->name('orden.index');
        //Route::get('/load', 'OrdenController@load')->name('orden.load');
        Route::get('/{orden}', 'OrdenController@show')->name('orden.show');
        Route::get('/crear/cliente/{cliente}', 'OrdenController@create')->name('orden.create');
        Route::post('/crear', 'OrdenController@store')->name('orden.store');
        Route::post('/{orden}/precio', 'OrdenController@precio')->name('orden.precio');
        Route::post('/{orden}/estado', 'OrdenController@estadoUpdate')->name('orden.estadoupdate');
        Route::get('/{orden}/print', 'OrdenController@print')->name('orden.print');
        Route::get('/{orden}/edit', 'OrdenController@edit')->name('orden.edit');
        Route::post('/{orden}/update', 'OrdenController@update')->name('orden.update');

    });

    Route::group(['prefix' => '/usuarios'], function() {
        Route::get('/', 'UserController@index')->name('users.index');
        Route::get('/crear', 'UserController@create')->name('users.create');
        Route::post('/', 'UserController@store')->name('users.store');
        Route::get('/{user}', 'UserController@show')->name('users.show');
        Route::get('/{user}/editar', 'UserController@edit')->name('users.edit');
        Route::post('/{user}', 'UserController@update')->name('users.update');
        Route::delete('/{user}', 'UserController@destroy')->name('users.destroy');
    });

    Route::get('/acceso/{user}/cambiar', 'UserController@editPassword')->name('users.password.edit');
    Route::post('/acceso/{user}', 'UserController@updatePassword')->name('users.password.update');

    Route::group(['prefix' => '/historial'], function() {
        Route::get('/{orden}/load', 'HistoriaController@load')->name('historia.load');
    });




//    Route::get('/link1', function ()    {
//        // Uses Auth Middleware
//    });

    //Please do not remove this if you want adminlte:route and adminlte:link commands to works correctly.
    #adminlte_routes
});
