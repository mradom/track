@if ($flash_message = session('flash_message'))
  <div class="alert {{ $flash_message['type'] ? 'bg-'.$flash_message['type'] : '' }} alert-styled-left">
    <button class="close" type="button" data-dismiss="alert">
      <span>×</span>
      <span class="sr-only">Close</span>
    </button>
    {{ $flash_message['message'] }}
  </div>
@endif

@if (count($errors) > 0)
  <div class="alert bg-danger alert-styled-left">
    <button class="close" type="button" data-dismiss="alert">
      <span>×</span>
      <span class="sr-only">Close</span>
    </button>
    <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif
