@extends('layout', [
  'title' => 'Tipos',
  'content_title' => 'Tipo',
  'content_sub_title' => 'Edicion'
])

@section('content')

<div class="row">
  <div class="col-md-8">
    <div class="box box-primary">
        <form class="form-horizontal" action="{{ route('marcas.update',['marca' => $marca->id]) }}" method="POST">
            @method('PUT')
            @csrf
            <div class="box-header">Edicion de Marca</div>
            <div class="box-body">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" style="width:100px">Tipo</label>
                        <div class="col-sm-10">
                            <select name="tipo_id" id="tipo_id" class="form-control">
                                <option value="0" disabled selected="selected">Seleccione Tipo...</option>
                                @foreach ($tipos as $tipo)
                                    @if ($tipo->id == $marca->tipo_id)
                                        <option value="{{$tipo->id}}" selected="selected">{{$tipo->nombre}}</option>
                                    @else
                                        <option value="{{$tipo->id}}">{{$tipo->nombre}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" style="width:100px">Nombre</label>
                        <div class="col-sm-10">
                            <input type="text" name="nombre" id="nombre" class="form-control" value="{{$marca->nombre}}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Actualizar</button>
            </div>
        </form>
    </div>
  </div>
</div>
@endsection