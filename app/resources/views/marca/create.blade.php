@extends('layout', [
  'title' => 'Marca',
  'content_title' => 'Marca',
  'content_sub_title' => 'Nueva'
])

@section('content')

<div class="row">
  <div class="col-md-8">
    <div class="box box-primary">
        <form class="form-horizontal" action="{{ route('marcas.store') }}" method="POST">
            {{ csrf_field() }}
            <div class="box-header">Nueva Marca</div>
            <div class="box-body">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" style="width:100px">Tipo</label>
                        <div class="col-sm-10">
                            <select name="tipo_id" id="tipo_id" class="form-control">
                                <option value="0" disabled selected="selected">Seleccione Tipo...</option>
                                @foreach ($tipos as $tipo)
                                    <option value="{{$tipo->id}}">{{$tipo->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" style="width:100px">Nombre</label>
                        <div class="col-sm-10">
                            <input type="text" name="nombre" id="nombre" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Crear</button>
            </div>
        </form>
    </div>
  </div>
</div>
@endsection

@push('body-scripts')
<script type="text/javascript">
  $(document).ready(() => {
    $("#tipo_id").select2({
            placeholder: 'Seleccione un tipo...',
      });
  });
</script>
@endpush