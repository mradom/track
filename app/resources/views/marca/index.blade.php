@extends('layout', [
  'title' => 'Marcas',
  'content_title' => 'Marcas',
  'content_sub_title' => 'Listado'
])

@section('content')

  <div class="row">
    <div class="col-xs-2 col-xs-offset-10">
      <a class="btn btn-block btn-primary" href="{{ route('marcas.create') }}">Crear Nuevo</a>
    </div>
  </div>
  <br>

<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-body">
        <table id="clientes" class="table table-bordered table-hover dataTable">
          <thead>
            <tr>
              <th>Tipo</th>
              <th>Nombre</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($marcas as $marca)
                <tr>
                    <td>{{ $marca->tipo->nombre }}</td>
                    <td>{{ $marca->nombre }}</td>
                    <td>
                        <a href="{{route('marcas.edit',['tipo' => $marca->id])}}"><i class='fa fa-edit'></i></a> 
                        <a href="{{route('marcas.destroy',['tipo' => $marca->id])}}"><i class="fa fa-remove"></i></a>
                    </td> 
                </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection