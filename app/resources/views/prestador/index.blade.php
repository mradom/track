@extends('layout', [
  'title' => 'Marcas',
  'content_title' => 'Marcas',
  'content_sub_title' => 'Listado'
])

@section('content')

  <div class="row">
    <div class="col-xs-2 col-xs-offset-10">
      <a class="btn btn-block btn-primary" href="{{ route('prestadores.create') }}">Crear Nuevo</a>
    </div>
  </div>
  <br>

<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-body">
        <table id="clientes" class="table table-bordered table-hover dataTable">
          <thead>
            <tr>
              <th>#</th>
              <th>Nombre</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($prestadores as $prestador)
                <tr>
                    <td>{{ $prestador->id }}</td>
                    <td>{{ $prestador->nombre }}</td>
                    <td>
                        <a href="{{route('prestadores.edit',['id' => $prestador->id])}}"><i class='fa fa-edit'></i></a> 
                        <a href="{{route('prestadores.destroy',['id' => $prestador->id])}}"><i class="fa fa-remove"></i></a>
                    </td> 
                </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection