@extends('layout', [
  'title' => 'Prestador',
  'content_title' => 'Prestador',
  'content_sub_title' => 'Edicion'
])

@section('content')

<div class="row">
  <div class="col-md-8">
    <div class="box box-primary">
        <form class="form-horizontal" action="{{ route('prestadores.update',['prestador' => $prestador->id]) }}" method="POST">
            @method('PUT')
            @csrf
            <div class="box-header">Edicion de Prestador</div>
            <div class="box-body">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" style="width:100px">Nombre</label>
                        <div class="col-sm-10">
                            <input type="text" name="nombre" id="nombre" class="form-control" value="{{$prestador->nombre}}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Actualizar</button>
            </div>
        </form>
    </div>
  </div>
</div>
@endsection