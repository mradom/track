@extends('adminlte::layouts.app')

@section('htmlheader_title', $title)
@section('contentheader_title', $content_title)
@section('contentheader_description', $content_sub_title)

@section('main-content')
  @include('utils.messages')

  @yield('content')
@endsection
