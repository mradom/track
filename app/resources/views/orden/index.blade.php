@extends('layout', [
  'title' => 'Ordenes',
  'content_title' => 'Ordenes',
  'content_sub_title' => 'Listado'
])

@section('content')

<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-body">
        <table id="ordenes" class="table table-bordered table-hover dataTable">
          <thead>
            <tr>
              <th>Orden</th>
              <th>Estado</th>
              <th>Cliente</th>
              <!-- <th>Apellido</th> -->
              <th>Modelo</th>
              <th>Color</th>
              <th>IMEI</th>
              <th>Falla</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection

<select name="estado" id="estado" class="form-control"></select>

@push('body-scripts')
<script type="text/javascript">
  function addHeaderInputsToHeader(datatable) {
    let row = '<tr>';
    datatable.columns().every(function (index) {
        // let column = datatable.column($(this).parent().index() + ':visible');
        // let idx = column.index();
        let settings = datatable.settings()[0].aoColumns[index];
        row += '<th>';
        if (settings.bSearchable) {
          if (settings.sTitle == "Estado") {
            row += '<select class="form-control" id="estado"><option value="" selected="selected">Todos</option> <option value="1">Ingreso</option> <option value="2">Diagnostico</option> <option value="4">Presupuesto</option> <option value="5">Presupuesto Aprobado</option> <option value="7">Pendiente por Material</option> <option value="8">Terminado Reparado</option> <option value="9">Terminado No Reparado</option> <option value="10">Entregado Reparado</option> <option value="11">Entregado No Reparado</option></select>'
          }else{
            if (settings.sTitle == "Orden" || settings.sTitle == "Color") {
              row += '<input type="text" placeholder="' + settings.sTitle + '" class="form-control" style="width:110px;" />'
            }else{
              row += '<input type="text" placeholder="' + settings.sTitle + '" class="form-control" />'
            }
          }
        }
        row += '</th>';
    });
    row += '</tr>';

    let $header = $(datatable.table().header());
    $header.prepend(row);
    $header.on('keyup change', 'input.form-control, select.form-control', _.debounce(function () {
        let input = this;
        datatable.column($(input).parent().index() + ':visible')
            .search(input.value)
            .draw();
    }, 500));
  }

  $(document).ready(() => {
    let datatable = $('#ordenes').DataTable({
      'processing': true,
      'serverSide': true,
      'ajax': {
        'url': '{{ route('orden.load') }}',
        'type': 'GET'
      },
      'columns': [
        {data: 'id', searchable: true},
        {data: 'estado_id', searchable: true},
        {data: 'cliente_rel.nombrecompleto', searchable: true},
        //{data: 'cliente', searchable: true},
        {data: 'modelo_rel.nombrecompleto', searchable: true},
        {data: 'color', searchable: true},
        {data: 'esn', searchable: true},
        {data: 'falla', searchable: true},
      ],
      'columnDefs': [
        {
          render: (data, type, row) => {
              return '<a href="' + laroute.route('orden.show', { orden: row.id }) + '">' + row.id + '</a>';
          },
          'targets': [0]
        },
        {
          render: (data, type, row) => {
              return row.estado_rel.nombre;
          },
          'targets': [1]
        },
      ]
    });
    addHeaderInputsToHeader(datatable);
  });
</script>
@endpush
