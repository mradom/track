@extends('layout', [
  'title' => 'Orden',
  'content_title' => 'Orden',
  'content_sub_title' => $orden->id,
])

@section('content')

<div class="row form-horizontal">
    <div class="col-md-8">
        <form action="{{route('orden.update',['orden'=>$orden->id])}}" method="POST">
            @csrf
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Modificación de Orden</h3></div>
                <div class="box-body">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Equipo</label>
                            <div class="col-sm-9">
                                <input type="text" name="" value="{{ $orden->modeloRel->nombrecompleto }}" class="form-control" disabled>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">IMEI/Serial</label>
                            <div class="col-sm-5">
                                <input type="text" name="esn" id="esn" value="{{ $orden->esn }}" class="form-control">
                            </div>
                            <label class="col-sm-1 control-label">Color</label>
                            <div class="col-sm-3">
                                <input type="text" name="color" id="color" value="{{ $orden->color }}" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Falla</label>
                            <div class="col-sm-9">
                                <textarea rows="2" class="form-control" name="falla" id="falla">{{ $orden->falla }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Accesorios</label>
                            <div class="col-sm-9">
                                <textarea rows="2" class="form-control" id="accesorios" name="accesorios">{{ $orden->accesorios }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-info pull-right" >Actualizar</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
