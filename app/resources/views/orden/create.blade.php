@extends('layout', [
  'title' => 'Orden',
  'content_title' => 'Orden',
  'content_sub_title' => 'Nueva'
])

@section('content')

<form class="form-horizontal" action="{{ route('orden.store') }}" method="POST">
    {{ csrf_field() }}
    <input type="hidden" name="cliente_id" id="cliente_id" value="{{ $cliente->id }}">

    <div class="box box-primary box-solid">
        <div class="box-body">

            <div class="row">
                <div class="col-md-7">
                    <cliente-ficha
                    :cliente="{{ $cliente }}"
                    ></cliente-ficha>

                    <div class="box box-primary box-solid">
                        <div class="box-header with-border">Nueva Orden</div>
                        <div class="box-body">
                            <div class="row">
                                <div class="form-group {{$errors->has('tipo_id') ? 'has-error' : ''}}">
                                    <label class="col-sm-2 control-label">Tipo</label>
                                    <div class="col-sm-9">
                                        <select name="tipo_id" id="tipo_id" class="form-control">
                                            <option value="0" disabled selected="selected">Seleccione Tipo...</option>
                                            @foreach ($tipos as $tipo)
                                                <option value="{{$tipo->id}}" {{ old('tipo_id') == $tipo->id ? 'selected' : '' }}>{{$tipo->nombre}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group {{$errors->has('marca_id') ? 'has-error' : ''}}">
                                    <label class="col-sm-2 control-label">Marca</label>
                                    <div class="col-sm-9">
                                        <select name="marca_id" id="marca_id" class="form-control">
                                            <option value="0" disabled selected="selected">Seleccione Marca...</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group {{$errors->has('modelo_id') ? 'has-error' : ''}}">
                                    <label class="col-sm-2 control-label">Modelo</label>
                                    <div class="col-sm-9">
                                        <select name="modelo_id" id="modelo" class="form-control">
                                            <option value="0" disabled selected="selected">Seleccione Modelo...</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group {{$errors->has('otro') ? 'has-error' : ''}}">
                                    <label class="col-sm-2 control-label" for="otro">Otro equipo <input type="checkbox" value="visible" name="chkotro" id="chkotro"></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="otro" id="otro" class="form-control" placeholder="Si la marca o modelo que busca no existe, ingrese acá!" readonly="readonly" value="{{ old('otro') }}">
                                    </div>
                                </div>
                                <div class="form-group {{$errors->has('esn') ? 'has-error' : ''}}">
                                    <label class="col-sm-2 control-label">IMEI / SERIAL <input type="checkbox" value="visible" name="chkvisible" id="chkvisible"></label>
                                    <div class="col-sm-5">
                                        <input type="text" name="esn" id="esn" class="form-control" value="{{ old('esn') }}">
                                    </div>
                                    <label class="col-sm-1 control-label" for="color">Color</label>
                                    <div class="col-sm-3">
                                        <input type="text" name="color" id="color" class="form-control" value="{{old('color')}}">
                                    </div>
                                </div>
                                <div class="form-group {{$errors->has('falla') ? 'has-error' : ''}}">
                                    <label class="col-sm-2 control-label" for="falla">Falla</label>
                                    <div class="col-sm-9">
                                        <textarea rows="2" class="form-control" name="falla" id="falla">{{old('falla')}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group {{$errors->has('accesorios') ? 'has-error' : ''}}">
                                    <label class="col-sm-2 control-label">Accesorios <input type="checkbox" value="sinaccesorios" name="chkaccesorios" id="chkaccesorios" checked="checked"></label>
                                    <div class="col-sm-9">
                                        <textarea rows="2" class="form-control" id="accesorios" name="accesorios" readonly="readonly">Sin Accesorios</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">

                    @include('orden.partials.diagnosticar')

                    @include('orden.partials.garantia')

                    @include('orden.partials.prestador')

                </div>
            </div>
        </div>
        <div class="box-footer">
            <a href="/cliente"><button type="button" class="btn btn-default">Cancelar</button></a>
            <button type="submit" class="btn btn-info pull-right">Crear</button>
        </div>
    </div>
</form>
@endsection

@push('body-scripts')
<script type="text/javascript">
  $(document).ready(() => {

    function loadMarcas()
    {
        if ($("#tipo_id").val() == 1)
        {
            $("#esn").attr('maxlength',15);
        }else{
            $("#esn").removeAttr('maxlength');
        }

        axios.get(laroute.route('marcas.tipo', { tipo: $("#tipo_id").val() }))
        .then((resp) => {

            $("#marca_id").children().remove();

            var listitems = "<option value='0' disabled selected='selected'>Seleccione Marca...</option>";
            var jsMarca = '{{ old('marca_id') }}';

            $.each(resp.data, function(i, value){
                if (jsMarca == value.id)
                {
                    listitems += "<option value='"+value.id+"' selected='selected'>"+value.nombre+"</option>";
                }else{
                    listitems += "<option value='"+value.id+"'>"+value.nombre+"</option>";
                }
            });
            $("#marca_id").append(listitems);
            $("#marca_id").select2();

        });
    }

    function loadModelos()
    {
        if ("{{old('marca_id')}}" != "") { var jsMarca = '{{ old('marca_id') }}'; }else{ var jsMarca = $("#marca_id").val();}

        axios.get(laroute.route('tipos.marcas.modelos', { tipo: $("#tipo_id").val() ,marca: jsMarca }))
        .then((resp) => {

            $("#modelo").children().remove();

            var listitems = "<option value='0' disabled selected='selected'>Seleccione Modelo...</option>";
            var jsModelo = '{{ old('modelo_id') }}';
            
            $.each(resp.data, function(i, value){
                if (jsModelo == value.id) {
                    listitems += "<option value='"+value.id+"' selected='selected'>"+value.nombre+"</option>";
                }else{
                    listitems += "<option value='"+value.id+"'>"+value.nombre+"</option>";
                }
                
            });
            $("#modelo").append(listitems);
            $("#modelo").select2();

        });
    }

    $("#chkaccesorios" ).on( "click", function(){
        if ($("#chkaccesorios").is(':checked')) {
            $("#accesorios").val("Sin Accesorios");
            $("#accesorios").attr("readonly",true);
        }else{
            $("#accesorios").val("");
            $("#accesorios").attr("readonly",false);
        }
    } );

    $("#chkvisible" ).on( "click", function(){
        if ($("#chkvisible").is(':checked')) {
            $("#esn").val("NO VISIBLE");
            $("#esn").attr("readonly",true);
        }else{
            $("#esn").val("");
            $("#esn").attr("readonly",false);
        }
    } );

    $("#chkotro" ).on( "click", function(){
        if (!$("#chkotro").is(':checked')) {
            $("#otro").attr("readonly",true);
        }else{
            $("#otro").val("");
            $("#otro").attr("readonly",false);
        }
    } );

    $("#tipo_id").select2({
            placeholder: 'Seleccione un tipo...',
      });
    $("#marca_id").select2({
            placeholder: 'Seleccione una marca...',
      });
    $("#modelo").select2({
            placeholder: 'Seleccione un modelo...',
      });

    $("#tipo_id").on('select2:select', function (e) {
        loadMarcas();
    });

    $("#marca_id").on('select2:select', function (e) {
        loadModelos();
    });

    $("#modelo").on('select2:select', function (e) {
        if ($("#modelo option:selected").text() == "Otro")
        {
            $("#otro").val("");
            $("#otro").focus();
            $("#otro").attr("readonly",false);
            $("#chkotro").prop('checked', true);
        }
    });

    $("#garantia").on('change', function (e) {
        if ($("#garantia").val() == 2 || $("#garantia").val() == 3)
        {
            $("#aseguradora").attr("readonly",false);
            $("#fecha").attr("readonly",false);
            $("#lugar").attr("readonly",false);
            if ($("#garantia").val() == 3) 
            {
                $("#norden").attr("readonly",false);
            }
        }else{
            $("#aseguradora").attr("readonly",true);
            $("#fecha").attr("readonly",true);
            $("#lugar").attr("readonly",true);
            $("#norden").attr("readonly",true);
        }
    });

    var jsFecha = '{{old('fecha')}}';
    var jsLugar = '{{old('lugar')}}';
    var jsNorden = '{{old('norden')}}';
    var jsOtro = '{{ old('otro') }}';
    var jsTipo = '{{ old('tipo_id') }}';
    var jsMarca = '{{ old('marca_id') }}';

    if (jsFecha != '' ) { $("#fecha").attr("readonly",false); }
    if (jsLugar != '' ) { $("#lugar").attr("readonly",false); }
    if (jsNorden != '' ) { $("#norden").attr("readonly",false); }
    if (jsOtro != '' ) { $("#otro").attr("readonly",false); $("#chkotro").prop("checked", true); }
    if (jsTipo != '' ) { loadMarcas(); }
    if (jsMarca != '' ) { loadModelos(); }


  });
</script>
@endpush