<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style>
	    body{
	      position: fixed;
	      font-family: sans-serif;
	    }
	    @page {
	      margin: 5px 5px 5px 5px;*/
	    }
	    table{
	    	font-size: 12px;
	    }
	    b{
	    	font-size: 12px;
	    }
		.date{
			font-size: 10px;
	    }
	</style>
</head>
<body style="margin:0;padding: 0;">
	<pre style="font-size:8px;font-weight: bold;">
  __ _| | |                     
 / _` | | |                     
| (_| | | |                     
 \__,_|_|_|                     
                                
                 _     _ _      
 _ __ ___   ___ | |__ (_) | ___ 
| '_ ` _ \ / _ \| '_ \| | |/ _ \
| | | | | | (_) | |_) | | |  __/
|_| |_| |_|\___/|_.__/|_|_|\___|

	</pre>
	<hr>
<b>Fecha y Hora: {{ date($orden->created_at) }}</b>
<h3>Orden: {{ $orden->id }}</h3>

<table>
	<tr>
		<th>Datos del quipo</th>
	</tr>
	<tr>
		<td>
			Tipo: {{ $equipo->marca->tipo->nombre }}
		</td>
	</tr>
	<tr>
		<td>
			Marca: {{ $equipo->marca->nombre }}
		</td>
	</tr>
	<tr>
		<td>
			Modelo: {{ $equipo->nombre }}
		</td>
	</tr>
	<tr>
		<td>
			IMEI/Serial: {{ $orden->esn }}
		</td>
	</tr>
	<tr>
		<td>
			Color: {{ $orden->color }}
		</td>
	</tr>
	<tr>
		<td>
			Falla: {{ $orden->falla }}
		</td>
	</tr>
	<tr>
		<td>
			Accesorios: {{ $orden->accesorios }}
		</td>
	</tr>
	<tr>
		<td>
			Empresa: {{ $orden->prestador }}
		</td>
	</tr>
	<tr>
		<td>
			Estado: {{ $orden->estado }}
		</td>
	</tr>
</table>
<hr>
<table>
	<tr>
		<th>Garantia</th>
	</tr>
	<tr>
		<td>
			@switch($garantia->tipo)
			    @case(1)
			        Sin Garantia
			        @break

			    @case(2)
			        Con Garantia <br>
			        {{ $garantia->fecha }} <br>
			        {{ $garantia->lugar }} 
			        @break

			    @case(3)
			        Con Garantia Extendida <br>
			        {{ $garantia->fecha }} <br>
			        {{ $garantia->lugar }} 
			        {{ $garantia->norden }}
			        @break

			    @case(4)
			        Con Garantia de la casa
			        @break

			@endswitch
		</td>
	</tr>
</table>

	@if ($orden->diagnostico != "" and $orden->precio != "")
		<hr>
		<table>
			<tr>
				<th>Diagnostico:</th>
			</tr>
			<tr>
				<td>{{ $orden->diagnostico }}</td>
			</tr>
			<tr>
				<th>Presupuesto:</th>
			</tr>
			<tr>
				<td>$ {{$orden->precio}}</td>
			</tr>
		</table>
	@endif

	<table>
		<tr>
			<th>Datos del cliente:</th>
		</tr>
		<tr>
			<td>
				{{ $cliente->apellido }}, {{ $cliente->nombre }}
				<br>
				Tel: {{ $cliente->telefono }}
				<br>
				Email: {{ $cliente->email }}
			</td>
		</tr>
	</table>

	<table>
		<tr>
			<td>
				<img src="img/patron.jpg" width="180">
			</td>
		</tr>
	</table>

	@if ($orden->estado_id == 8 || $orden->estado_id == 9 )
		<table class="tabla" style="border:1px solid #000000">
			<tr>
				<td rowspan="0" colspan="0"><b>EGRESO EN CONFORMIDAD</b></td>
			</tr>
			<tr>
				<td rowspan="0" colspan="0">Firma y Aclaración</td>
			</tr>
			<tr>
				<td rowspan="0" colspan="0" style="height: 150px"></td>
			</tr>
			<tr>
				<td><span class="date">Fecha: {{ date('d/m/Y H:i') }}</span></td>
			</tr>
		</table>
	@else
		<table class="tabla" style="border:1px solid #000000">
			<tr>
				<td rowspan="0" colspan="0"><b>INGRESO EN CONFORMIDAD</b></td>
			</tr>
			<tr>
				<td rowspan="0" colspan="0">Firma y Aclaración</td>
			</tr>
			<tr>
				<td rowspan="0" colspan="0" style="height: 150px"></td>
			</tr>
		</table>
	@endif
	<br>
	<br>
	<br>
</body>
</html>