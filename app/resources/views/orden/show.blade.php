@extends('layout', [
  'title' => 'Orden',
  'content_title' => 'Orden',
  'content_sub_title' => $orden->id,
])

@section('content')

<div class="row form-horizontal">
    <div class="col-md-8">
        <cliente-ficha
        :cliente="{{ $orden->clienteRel }}"
        ></cliente-ficha>

        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Detalle de Orden</h3></div>
            <div class="box-body">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Equipo</label>
                        <div class="col-sm-9">
                            <input type="text" name="" value="{{ $orden->modeloRel->nombrecompleto }}" class="form-control" disabled>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">IMEI/Serial</label>
                        <div class="col-sm-5">
                            <input type="text" name="esn" id="esn" value="{{ $orden->esn }}" class="form-control" disabled>
                        </div>
                        <label class="col-sm-1 control-label">Color</label>
                        <div class="col-sm-3">
                            <input type="text" name="color" id="color" value="{{ $orden->color }}" class="form-control" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Falla</label>
                        <div class="col-sm-9">
                            <textarea rows="2" class="form-control" name="falla" id="falla" disabled>{{ $orden->falla }}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Accesorios</label>
                        <div class="col-sm-9">
                            <textarea rows="2" class="form-control" id="accesorios" name="accesorios" readonly="readonly">{{ $orden->accesorios }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <a class="btn btn-info pull-right" href="{{route('orden.edit',['orden'=>$orden->id])}}" >Editar</a>
            </div>
        </div>

        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Historial</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                @include('orden.partials.historial')
            </div>
        </div>
    </div>
    <div class="col-md-4">

        <div class="box box-primary box-solid">
            <div class="box-header">Imprimir</div>
            <div class="box-body">
                <a href="{{route('orden.print',['orden'=>$orden->id])}}" target="_BLANK">
                    <img src="/img/pdf-128.png" width="64px">
                </a>
            </div>
        </div>

        <div class="box box-primary box-solid">
            <form action="{{ route('fotos.store') }}" method="POST" enctype="multipart/form-data">
            <div class="box-header">Foto</div>
            <div class="box-body">
                {{ csrf_field() }}
                <input type="hidden" name="orden" value="{{ $orden->id }}">
                <input type="file" name="foto">
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Guardar</button>
            </div>
            </form>
        </div>

        <div class="box box-primary box-solid">
            <div class="box-header">Información Extra</div>
            <div class="box-body">
                <p><b>Prestador: </b>{{ $orden->prestador }}</p>
                <p><b>Clave: </b>{{ $orden->clave }}</p>
                <p><b>Estado: </b>{{ $orden->estado }}</p>
            </div>
        </div>

        <form action="{{route('orden.estadoupdate',['orden'=> $orden->id])}}" method="POST">
            <div class="box box-primary box-solid">
                {{ csrf_field() }}
                <div class="box-header">Estado Actual <small class="label label-warning"></i>{{ $orden->estadoRel->nombre }}</small></div>
                <div class="box-body">

                    <select name="estado" id="estado" class="form-control">
                        <option value="0" disabled="disabled" selected="selected">Seleccione estado nuevo</option>
                        @foreach ($estados as $estado)
                            @if ($orden->estado_id == $estado->id)
                                <option value="{{ $estado->id }}" selected="selected">{{ $estado->nombre }}</option>
                            @else
                                <option value="{{ $estado->id }}">{{ $estado->nombre }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="box-body precio-frame">
                    <div class="form-group {{$errors->has('diagnostico') ? 'has-error' : ''}}">
                        <label class="col-sm-3 control-label">Diagnostico</label>
                        <div class="col-sm-8">
                            <textarea rows="3" name="diagnostico" id="diagnostico" class="form-control" readonly="true">{{ $orden->diagnostico }}</textarea>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">$</span>
                        <input type="text" name="precio" id="precio" class="form-control" value="{{ $orden->precio }}" readonly="true">
                        <span class="input-group-addon">.00</span>
                    </div>
                </div>
                <div class="box-footer">
                    @if ($orden->estado_id < 8)
                        <button type="submit" class="btn btn-info pull-right">Guardar</button>
                    @endif
                </div>
            </div>
        </form>

        <div class="box box-primary box-solid">
            <div class="box-header">{{ $orden->garantia->nombre }}</div>
            @if ($orden->garantia->tipo > 1)
            <div class="box-body">
                <p><b>Lugar de compra: </b>{{ $orden->garantia->lugar }}</p>
                @if ($orden->garantia->aseguradora)
                    <p><b>Prestador: </b>{{ $orden->garantia->aseguradora->nombre }}</p>
                @endif
                <p><b>Fecha de compra: </b>{{ $orden->garantia->fecha }}</p>
            </div>
            @endif
        </div>

        <!-- <div class="box box-primary">
            <div class="box-header">Fotos</div>
            <div class="box-body">
                FOTOS
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Crear</button>
            </div>
        </div> -->

    </div>
</div>
@endsection


@push('body-scripts')
<script type="text/javascript">
    $(document).ready(() => {

        $( "#estado" ).change(function() {
            if ($("#estado").val() == 3) {
                $("#precio").attr('readonly', false);
                $("#diagnostico").attr('readonly', false);
            }else{
                $("#precio").attr('readonly', true);
                $("#diagnostico").attr('readonly', true);
            }
        });

    });
</script>
@endpush