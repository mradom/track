<table id="historial" class="table table-bordered table-hover dataTable">
  <thead>
    <tr>
      <th>ID</th>
      <th>Fecha</th>
      <th>Accion</th>
      <th>Usuario</th>
      <th>Mensaje</th>
    </tr>
  </thead>
  <tbody>
  </tbody>
</table>

@push('body-scripts')
<script type="text/javascript">
  $(document).ready(() => {
    $('#historial').DataTable({
      'processing': true,
      'serverSide': true,
      'ajax': {
        'url': '{{ route('historia.load', ['orden' => $orden->id]) }}',
        'type': 'GET'
      },
      'columns': [
        {data: 'id'},
        {data: 'created_at'},
        {data: 'accion'},
        {data: 'usuario.name'},
        {data: 'mensaje'},
      ],
    });
  });
</script>
@endpush