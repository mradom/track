<div class="box box-primary box-solid">
    <div class="box-header with-border">Diagnosticar ahora ?</div>
    <div class="box-body">
        <div class="row">
            <div class="form-group {{$errors->has('diagnostico') ? 'has-error' : ''}}">
                <label class="col-sm-3 control-label">Diagnostico</label>
                <div class="col-sm-8">
                    <textarea rows="3" name="diagnostico" id="diagnostico" class="form-control">{{old('diagnostico')}}</textarea>
                </div>
            </div>
            <div class="form-group {{$errors->has('precio') ? 'has-error' : ''}}">
                <label class="col-sm-3 control-label">Precio</label>
                <div class="col-sm-8">
                    <input type="text" name="precio" id="precio" class="form-control" value="{{old('precio')}}">
                </div>
            </div>
        </div>
    </div>
</div>