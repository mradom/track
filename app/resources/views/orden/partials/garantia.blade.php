<div class="box box-primary box-solid">
    <div class="box-header with-border">Garantia</div>
    <div class="box-body">
        <div class="row">
            <div class="form-group {{$errors->has('garantia') ? 'has-error' : ''}}">
                <label class="col-sm-3 control-label">Tipo</label>
                <div class="col-sm-8">
                    <select name="garantia" id="garantia" class="form-control">
                        <option value="0" disabled selected="selected">Seleccione Tipo...</option>
                        <option value="1" {{ old('garantia') == 1 ? 'selected' : '' }}>Sin Garantia</option>
                        <option value="2" {{ old('garantia') == 2 ? 'selected' : '' }}>Con Garantia</option>
                        <option value="3" {{ old('garantia') == 3 ? 'selected' : '' }}>Con Garantia Extendida</option>
                        <option value="4" {{ old('garantia') == 4 ? 'selected' : '' }}>Garantia de la Casa</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Aseguradora</label>
                <div class="col-sm-8">
                    <select id="aseguradora" name="aseguradora" class="form-control" readonly="readonly">
                        <option value="0" disabled selected="selected">Seleccione Prestador...</option>
                        @foreach ($prestadores as $prestador)
                            <option value="{{ $prestador->id }}">{{ $prestador->nombre }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Fecha</label>
                <div class="col-sm-8">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" name="fecha" id="fecha" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" placeholder="dd/mm/aaaa" readonly="readonly" data-mask="" value="{{old('fecha')}}">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Lugar de compra</label>
                <div class="col-sm-8">
                    <input type="text" name="lugar" id="lugar" class="form-control" readonly="readonly" value="{{old('lugar')}}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">N° de Orden</label>
                <div class="col-sm-8">
                    <input type="text" name="norden" id="norden" class="form-control" readonly="readonly" value="{{old('norden')}}">
                </div>
            </div>
        </div>
    </div>
</div>