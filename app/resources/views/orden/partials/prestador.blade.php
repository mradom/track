<div class="box box-primary box-solid">
    <div class="box-header with-border">Datos del equipo</div>
    <div class="box-body">
        <div class="row">
            <div class="form-group {{$errors->has('prestador') ? 'has-error' : ''}}">
                <label class="col-sm-3 control-label">Prestador</label>
                <div class="col-sm-8">
                    <select name="prestador" id="prestador" class="form-control">
                        <option value="0" disabled selected="selected">Seleccione Prestador...</option>
                        <option value="Claro" {{ old('prestador') == 'Claro' ? 'selected' : '' }}>Claro</option>
                        <option value="Movistar" {{ old('prestador') == 'Movistar' ? 'selected' : '' }}>Movistar</option>
                        <option value="Personal" {{ old('prestador') == 'Personal' ? 'selected' : '' }}>Personal</option>
                        <option value="Otro" {{ old('prestador') == 'Otro' ? 'selected' : '' }}>Otro</option>
                    </select>
                </div>
            </div>
            <div class="form-group {{$errors->has('clave') ? 'has-error' : ''}}">
                <label class="col-sm-3 control-label">Contraseña <input type="checkbox" value="sinclave" name="chkclave" id="chkclave" checked="checked"></label>
                <div class="col-sm-8">
                    <input type="text" name="clave" id="clave" class="form-control" value="{{old('clave')}}">
                </div>
            </div>

            <div class="form-group {{$errors->has('estado') ? 'has-error' : ''}}">
                <label class="col-sm-3 control-label">Estado</label>
                <div class="col-sm-8">
                    <input type="text" name="estado" id="estado" class="form-control" value="{{old('estado')}}">
                </div>
            </div>
        </div>
    </div>
</div>


@push('body-scripts')
<script type="text/javascript">
  $(document).ready(() => {

    $("#clave").val("Sin Clave");
    $("#clave").attr("readonly",true);

    $("#chkclave" ).on( "click", function(){
        if ($("#chkclave").is(':checked')) {
            $("#clave").val("Sin Clave");
            $("#clave").attr("readonly",true);
        }else{
            $("#clave").val("");
            $("#clave").attr("readonly",false);
        }
    } );


  });
</script>
@endpush