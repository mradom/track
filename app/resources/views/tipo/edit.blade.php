@extends('layout', [
  'title' => 'Tipos',
  'content_title' => 'Tipo',
  'content_sub_title' => 'Edicion'
])

@section('content')

<div class="row">
  <div class="col-xs-6">
    <div class="box box-primary">
        <form class="form-horizontal" action="{{ route('tipos.update',['tipo' => $tipo->id]) }}" method="POST">
            @method('PUT')
            @csrf
            <div class="box-header">Edicio de Tipo</div>
            <div class="box-body">
                <div class="row">
                    <div class="form-group col-sm-9">
                        <label class="control-label" style="width:100px">Nombre</label>
                        <input type="text" name="nombre" id="nombre" value="{{$tipo->nombre}}">
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Actualizar</button>
            </div>
        </form>
    </div>
  </div>
</div>
@endsection