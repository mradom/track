@extends('layout', [
  'title' => 'Tipos',
  'content_title' => 'Tipo',
  'content_sub_title' => 'Nuevo'
])

@section('content')

<div class="row">
  <div class="col-xs-6">
    <div class="box box-primary">
        <form class="form-horizontal" action="{{ route('tipos.store') }}" method="POST">
            {{ csrf_field() }}
            <div class="box-header">Nuevo Tipo</div>
            <div class="box-body">
                <div class="row">
                    <div class="form-group col-sm-9">
                        <label class="control-label" style="width:100px">Nombre</label>
                        <input type="text" name="nombre" id="nombre">
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Crear</button>
            </div>
        </form>
    </div>
  </div>
</div>
@endsection