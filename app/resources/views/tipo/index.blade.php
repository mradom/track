@extends('layout', [
  'title' => 'Tipos',
  'content_title' => 'Tipos',
  'content_sub_title' => 'Listado'
])

@section('content')

  <div class="row">
    <div class="col-xs-2 col-xs-offset-10">
      <a class="btn btn-block btn-primary" href="{{ route('tipos.create') }}">Crear Nuevo</a>
    </div>
  </div>
  <br>

<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-body">
        <table id="clientes" class="table table-bordered table-hover dataTable">
          <thead>
            <tr>
              <th>Nombre</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($tipos as $tipo)
                <tr>
                    <td>{{ $tipo->nombre }}</td>
                    <td>
                        <a href="{{route('tipos.edit',['tipo' => $tipo->id])}}"><i class='fa fa-edit'></i></a> 
                        <a href="{{route('tipos.destroy',['tipo' => $tipo->id])}}"><i class="fa fa-remove"></i></a>
                    </td> 
                </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection