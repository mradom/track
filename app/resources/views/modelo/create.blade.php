@extends('layout', [
  'title' => 'Modelo',
  'content_title' => 'Modelo',
  'content_sub_title' => 'Nuevo'
])

@section('content')

<div class="row">
  <div class="col-md-8">
    <div class="box box-primary">
        <form class="form-horizontal" action="{{ route('modelos.store') }}" method="POST">
            {{ csrf_field() }}
            <div class="box-header">Nuevo Modelo</div>
            <div class="box-body">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" style="width:100px">Tipo</label>
                        <div class="col-sm-10">
                            <select name="tipo_id" id="tipo_id" class="form-control">
                                <option value="0" disabled selected="selected">Seleccione Tipo...</option>
                                @foreach ($tipos as $tipo)
                                    <option value="{{$tipo->id}}">{{$tipo->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" style="width:100px">Marca</label>
                        <div class="col-sm-10">
                            <select name="marca_id" id="marca_id" class="form-control">
                                <option value="0" disabled selected="selected">Seleccione Marca...</option>

                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" style="width:100px">Nombre</label>
                        <div class="col-sm-10">
                            <input type="text" name="nombre" id="nombre" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Crear</button>
            </div>
        </form>
    </div>
  </div>
</div>
@endsection

@push('body-scripts')
<script type="text/javascript">
  $(document).ready(() => {
    $("#tipo_id").select2({
            placeholder: 'Seleccione un tipo...',
      });

    $("#tipo_id").on('select2:select', function (e) {
        axios.get(laroute.route('marcas.tipo', { tipo: e.params.data.id }))
        .then((resp) => {

            $("#marca_id").children().remove();

            var listitems = "<option value='0' disabled selected='selected'>Seleccione Marca...</option>";
            $.each(resp.data, function(i, value){
                listitems += "<option value='"+value.id+"'>"+value.nombre+"</option>";
            });
            $('#cursos').select2("destroy");
            $("#marca_id").append(listitems);
            $("#marca_id").select2();

        });

      });
  });
</script>
@endpush