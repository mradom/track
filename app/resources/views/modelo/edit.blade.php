@extends('layout', [
  'title' => 'Modelo',
  'content_title' => 'Modelo',
  'content_sub_title' => 'Edicion'
])

@section('content')

<div class="row">
  <div class="col-md-8">
    <div class="box box-primary">
        <form class="form-horizontal" action="{{ route('modelos.update',['modelo' => $modelo->id]) }}" method="POST">
            @method('PUT')
            @csrf
            <div class="box-header">Edicion de Modelo</div>
            <div class="box-body">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" style="width:100px">Marca</label>
                        <div class="col-sm-10">
                            <select name="marca_id" id="marca_id" class="form-control">
                                <option value="0" disabled selected="selected">Seleccione Marca...</option>
                                @foreach ($marcas as $marca)
                                    @if ($marca->id == $modelo->marca_id)
                                        <option value="{{$marca->id}}" selected="selected">{{$marca->nombre}}</option>
                                    @else
                                        <option value="{{$marca->id}}">{{$marca->nombre}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" style="width:100px">Nombre</label>
                        <div class="col-sm-10">
                            <input type="text" name="nombre" id="nombre" class="form-control" value="{{$modelo->nombre}}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Actualizar</button>
            </div>
        </form>
    </div>
  </div>
</div>
@endsection