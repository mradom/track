@extends('layout', [
  'title' => 'Modelos',
  'content_title' => 'Modelos',
  'content_sub_title' => 'Listado'
])

@section('content')

  <div class="row">
    <div class="col-xs-2 col-xs-offset-10">
      <a class="btn btn-block btn-primary" href="{{ route('modelos.create') }}">Crear Nuevo</a>
    </div>
  </div>
  <br>

<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-body">
        <table id="clientes" class="table table-bordered table-hover dataTable">
          <thead>
            <tr>
              <th>Marca</th>
              <th>Nombre</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($modelos as $modelo)
                <tr>
                    <td>{{ $modelo->marca->nombre }}</td>
                    <td>{{ $modelo->nombre }}</td>
                    <td>
                        <a href="{{route('modelos.edit',['tipo' => $modelo->id])}}"><i class='fa fa-edit'></i></a> 
                        <a href="{{route('modelos.destroy',['tipo' => $modelo->id])}}"><i class="fa fa-remove"></i></a>
                    </td> 
                </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection