@extends('layout', [
  'title' => 'Clientes',
  'content_title' => 'Clientes',
  'content_sub_title' => 'Nuevo'
])

@section('content')

<div class="row">
  <div class="col-md-8">
    <form class="form-horizontal" method="POST" action="{{ route('cliente.update',['cliente'=>$cliente->id]) }}">
        {{ csrf_field() }}
        <input type="hidden" name="tipo" value="{{ $cliente->tipo }}">
    <div class="box box-primary">
        <div class="box-header">Edicion de Cliente</div>
        <div class="box-body">
            <div class="row">
                @if ($cliente->tipo == 1)
                    <div class="form-group">
                        <label class="col-sm-2 control-label" style="width:100px">DNI</label>
                        <div class="col-sm-8">
                            <input type="number" class="form-control" name="dni" id="dni" value="{{ $cliente->dni }}" readonly="true">
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" style="width:100px">Apellido</label>
                        <div class="col-sm-10">
                            <input type="text" name="apellido" id="apellido" class="form-control" value="{{ $cliente->apellido }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" style="width:100px">Nombre</label>
                        <div class="col-sm-10">
                            <input type="text" name="nombre" id="nombre" class="form-control" value="{{ $cliente->nombre }}">
                        </div>
                    </div>
                @else
                    <div class="form-group">
                        <label class="col-sm-2 control-label" style="width:100px">CUIT</label>
                        <div class="col-sm-8">
                            <input type="number" class="form-control" name="dni" id="dni" value="{{ $cliente->dni }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" style="width:100px">Nombre Fantasia</label>
                        <div class="col-sm-10">
                            <input type="text" name="apellido" id="apellido" class="form-control" value="{{ $cliente->apellido }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" style="width:100px">Razon Social</label>
                        <div class="col-sm-10">
                            <input type="text" name="nombre" id="nombre" class="form-control" value="{{ $cliente->nombre }}">
                        </div>
                    </div>
                @endif
                <div class="form-group">
                    <label class="col-sm-2 control-label" style="width:100px">Telefono</label>
                    <div class="col-sm-10">
                        <input type="text" name="telefono" id="telefono" class="form-control" value="{{ $cliente->telefono }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" style="width:100px">Direccion</label>
                    <div class="col-sm-10">
                        <input type="text" name="direccion" id="direccion" class="form-control" value="{{ $cliente->direccion }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" style="width:100px">E-Mail</label>
                    <div class="col-sm-10">
                        <input type="text" name="email" id="email" class="form-control" value="{{ $cliente->email }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-info pull-right">Actualizar</button>
        </div>
    </div>
    </form>
  </div>
</div>
@endsection