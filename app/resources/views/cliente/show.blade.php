@extends('layout', [
  'title' => 'Clientes',
  'content_title' => 'Clientes',
  'content_sub_title' => 'Nuevo'
])

@section('content')

<div class="row">
  <div class="col-md-8">
                <cliente-ficha
        :cliente="{{ $cliente }}"
        ></cliente-ficha>
  </div>
</div>
@endsection