@extends('layout', [
  'title' => 'Clientes',
  'content_title' => 'Clientes',
  'content_sub_title' => 'Listado'
])

@section('content')

  <div class="row">
    <div class="col-xs-2 col-xs-offset-10">
      <a class="btn btn-block btn-primary" href="{{ route('cliente.crear') }}">Crear Nuevo</a>
    </div>
  </div>
  <br>

<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-body">
        <table id="clientes" class="table table-bordered table-hover dataTable">
          <thead>
            <tr>
              <th>Id</th>
              <th></th>
              <th>DNI</th>
              <th>Apellido</th>
              <th>Nombre</th>
              <th>Telefono</th>
              <th>Email</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection

@push('body-scripts')
<script type="text/javascript">
  $(document).ready(() => {
	$('#clientes').DataTable({
      'processing': true,
      'serverSide': true,
      'ajax': {
        'url': '{{ route('cliente.load') }}',
        'type': 'GET'
      },
      'columns': [
        {data: 'id'},
        {data: 'id', searchable: false},
        {data: 'dni', searchable: true},
        {data: 'apellido', searchable: true},
        {data: 'nombre', searchable: true},
        {data: 'telefono', searchable: true},
        {data: 'email', searchable: true}
      ],
      'columnDefs': [
        {
          render: (data, type, row) => {
            return '<a href="' + laroute.route('cliente.view', { cliente: row.id }) + '"><button type="button" class="btn btn-block btn-default btn-xs">'+row.id+'</button></a>';
          },
          'targets': [0]
        },
        {
          render: (data, type, row) => {
            return '<a href="' + laroute.route('orden.create', { cliente: row.id }) + '"><button type="button" class="btn btn-block btn-default btn-xs">OT</button></a>';
          },
          'targets': [1]
        },
      ],
      initComplete: function () {
          this.api().columns().every(function () {
              var column = this;
              var input = document.createElement("input");
              $(input).appendTo($(column.footer()).empty())
              .on('change', function () {
                  column.search($(this).val(), false, false, true).draw();
              });
          });
      }
    });
  });
</script>
@endpush
