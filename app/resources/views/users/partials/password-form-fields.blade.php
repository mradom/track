{{ csrf_field() }}
<div class="form-group">
  <label class="col-sm-2 control-label" for="current">Actual</label>
  <div class="col-sm-10">
    <input id="current" class="form-control" type="password" name="current" placeholder="Contraseña Actual" required value=""/>
  </div>
</div>
<div class="form-group">
  <label class="col-sm-2 control-label" for="new">Nueva</label>
  <div class="col-sm-10">
    <input id="new" class="form-control" type="password" name="new" placeholder="Contraseña Nueva" required value=""/>
  </div>
</div>
<div class="form-group">
  <label class="col-sm-2 control-label" for="repeat">Repetir</label>
  <div class="col-sm-10">
    <input id="repeat" class="form-control" type="password" name="repeat" placeholder="Repetir Contraseña" required value=""/>
  </div>
</div>
