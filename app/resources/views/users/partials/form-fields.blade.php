<input type="hidden" name="id" value="{{ $system_user->id }}"/>
{{ csrf_field() }}
<div class="form-group">
  <label class="col-sm-2 control-label" for="name">Nombre <span class="text-danger">*</span></label>
  <div class="col-sm-10">
    <input id="name" class="form-control" type="text" name="name" placeholder="Nombre" required value="{{ $system_user->name }}"/>
  </div>
</div>
<div class="form-group">
  <label class="col-sm-2 control-label" for="email">Email <span class="text-danger">*</span></label>
  <div class="col-sm-10">
    <input id="email" class="form-control" type="email" name="email" placeholder="Correo Electrónico" required value="{{ $system_user->email }}"/>
  </div>
</div>

@push('body-scripts')
<script type="text/javascript">
  $(document).ready(() => {
    $('#roles').select2();
  });
</script>
@endpush
