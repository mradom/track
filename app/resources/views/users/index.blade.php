@extends('layout', [
  'title' => 'Usuarios',
  'content_title' => 'Usuarios',
  'content_sub_title' => 'detalle'
])

@section('content')
  <div class="row">
    <div class="col-xs-2 col-xs-offset-10">
      <a class="btn btn-block btn-primary" href="{{ route('users.create') }}">Crear Nuevo</a>
    </div>
  </div>
  <br>
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-body">
        <table id="users" class="table table-bordered table-hover dataTable">
          <thead>
            <tr>
              <th>Id</th>
              <th>Nombre</th>
              <th>Correo Electrónico</th>
              <th>Fecha creación</th>
            </tr>
          </thead>
          <tbody>
            @foreach($users as $user)
            <tr>
              <td>{{ $user->id }}</td>
              <td>
                <a href="{{ route('users.edit', ['user' => $user]) }}">{{ $user->name }}</a>
              </td>
              <td>{{ $user->email }}</td>
              <td>{{ $user->created_at }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection

@push('body-scripts')
<script type="text/javascript">
  $(document).ready(() => {
    $('#users').DataTable();
  });
</script>
@endpush
