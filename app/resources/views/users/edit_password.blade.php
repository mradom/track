@extends('layout', [
  'title' => 'Usuarios',
  'content_title' => $system_user->name,
  'content_sub_title' => 'cambiar contraseña'
])

@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="box box-primary">
      <form class="form-horizontal" action="{{ route('users.password.update', ['user' => $system_user]) }}" method="POST">
        <div class="box-body">
            @include('users.partials.password-form-fields')
        </div>
        <div class="box-footer">
          <button class="btn btn-primary pull-right" type="submit">Enviar</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
