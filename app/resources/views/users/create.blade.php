@extends('layout', [
  'title' => 'Usuarios',
  'content_title' => 'Usuarios',
  'content_sub_title' => 'crear nuevo'
])

@section('content')
<div class="row">
  <div class="col-md-8">
    <div class="box box-primary">
      <form class="form-horizontal" action="{{ route('users.store') }}" method="POST">
        <div class="box-body">
            @include('users.partials.form-fields')
        </div>
        <div class="box-footer">
          <button class="btn btn-primary pull-right" type="submit">Enviar</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
