@extends('layout', [
  'title' => 'Usuarios',
  'content_title' => $system_user->name,
  'content_sub_title' => 'editar'
])

@section('content')
<div class="row">
  <div class="col-md-8">
    <div class="box box-primary">
      <form class="form-horizontal" action="{{ route('users.update', ['user' => $system_user]) }}" method="POST">
        <div class="box-body">
            @include('users.partials.form-fields')
        </div>
        <div class="box-footer">
          @if(logged_user('id') != $system_user->id)
            <a href="#" class="btn btn-danger">Eliminar</a>
          @endif
          <button class="btn btn-primary pull-right" type="submit">Enviar</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@push('body-scripts')
@if(logged_user('id') != $system_user->id)
<script type="text/javascript">
  $(document).ready(() => {
    $('.btn-danger').on('click', () => {
      let id = {{ $system_user->id }};
      axios.delete(laroute.route('users.destroy', { user: id }))
        .then((response) => {
          window.location = laroute.route('users.index');
        });
    });
  });
</script>
@endif
@endpush
