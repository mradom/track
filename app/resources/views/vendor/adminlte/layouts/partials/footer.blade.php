<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        <a href="http://www.codigitar.com"></a><b>Codigitar</b></a>
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2019.</strong>
</footer>
