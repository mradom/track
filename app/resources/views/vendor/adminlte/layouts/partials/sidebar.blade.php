<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ Gravatar::get($user->email) }}" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p style="overflow: hidden;text-overflow: ellipsis;max-width: 160px;" data-toggle="tooltip" title="{{ Auth::user()->name }}">{{ Auth::user()->name }}</p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i>Conectado</a>
                </div>
            </div>
        @endif

        <!-- search form (Optional) -->
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Menu</li>
            <!-- Optionally, you can add icons to the links -->
            <li class="{{ request()->is('cliente*') ? 'active' : '' }}"><a href="{{ route('cliente.index') }}"><i class='fa fa-users'></i> <span>Clientes</span></a></li>
            <li class="{{ request()->is('orden*') ? 'active' : '' }}"><a href="{{ route('orden.index') }}"><i class='fa fa-phone'></i> <span>Ordenes</span></a></li>
        </ul><!-- /.sidebar-menu -->
        <!-- Sidebar Menu Configuracion -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Configuracion</li>
            <li class="treeview {{ request()->is('usuarios*') || request()->is('acceso*') || request()->is('roles*') ? 'active menu-open' : '' }}">
              <a href="#">
                <i class='fa fa-sign-in'></i> <span>Acceso</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="{{ request()->is('usuarios*') ? 'active' : '' }}"><a href="{{ route('users.index') }}">Usuarios</a></li>
                <li class="{{ request()->is('acceso*') ? 'active' : '' }}"><a href="{{ route('users.password.edit', ['user' => logged_user()]) }}">Cambiar Contraseña</a>
                </li>
              </ul>
            </li>
            <li class="treeview {{ request()->is('tipos*') || request()->is('marcas*') || request()->is('modelos*') || request()->is('estados*') ? 'active menu-open' : '' }}">
              <a href="#">
                <i class='fa fa-gear'></i> <span>Configuración</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="{{ request()->is('tipos*') ? 'active' : '' }}"><a href="{{ route('tipos.index') }}"><i class='fa fa-cube'></i> <span>Tipos</span></a></li>
                <li class="{{ request()->is('marcas*') ? 'active' : '' }}"><a href="{{ route('marcas.index') }}"><i class='fa fa-cube'></i> <span>Marcas</span></a></li>
                <li class="{{ request()->is('modelos*') ? 'active' : '' }}"><a href="{{ route('modelos.index') }}"><i class='fa fa-cube'></i> <span>Modelos</span></a></li>
                <li class="{{ request()->is('estados*') ? 'active' : '' }}"><a href="{{ route('estados.index') }}"><i class='fa fa-cube'></i> <span>Estados</span></a></li>
                <li class="{{ request()->is('prestador*') ? 'active' : '' }}"><a href="{{ route('prestadores.index') }}"><i class='fa fa-cube'></i> <span>Prestadores Garantia</span></a></li>
              </ul>
            </li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
