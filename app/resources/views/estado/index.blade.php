@extends('layout', [
  'title' => 'Estados',
  'content_title' => 'Estados',
  'content_sub_title' => 'Listado'
])

@section('content')

  <div class="row">
    <div class="col-xs-2 col-xs-offset-10">
      <a class="btn btn-block btn-primary" href="{{ route('estados.create') }}">Crear Nuevo</a>
    </div>
  </div>
  <br>

<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-body">
        <table id="clientes" class="table table-bordered table-hover dataTable">
          <thead>
            <tr>
              <th>ID</th>
              <th>Nombre</th>
              <th>Orden</th>
              <th colspan="2">Acciones</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($estados as $estado)
                <tr>
                    <td>{{ $estado->id }}</td>
                    <td>{{ $estado->nombre }}</td>
                    <td>{{ $estado->orden }}</td>
                    <td>
                        @if (!$loop->first)
                            <a href="{{route('estado.up',['estado' => $estado->id])}}">
                                <i class="fa fa-arrow-circle-up"></i>
                            </a>
                        @endif
                    </td>
                    <td>
                        @if (!$loop->last)
                            <a href="{{route('estado.down',['estado' => $estado->id])}}">
                                <i class="fa fa-arrow-circle-down"></i>
                            </a>
                        @endif
                    </td>
                </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection