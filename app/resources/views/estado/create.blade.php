@extends('layout', [
  'title' => 'Estado',
  'content_title' => 'Estado',
  'content_sub_title' => 'Nuevo'
])

@section('content')

<div class="row">
  <div class="col-md-8">
    <div class="box box-primary">
        <form class="form-horizontal" action="{{ route('estados.store') }}" method="POST">
            {{ csrf_field() }}
            <div class="box-header">Estado Nuevo</div>
            <div class="box-body">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" style="width:100px">Nombre</label>
                        <div class="col-sm-10">
                            <input type="text" name="nombre" id="nombre" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Crear</button>
            </div>
        </form>
    </div>
  </div>
</div>
@endsection