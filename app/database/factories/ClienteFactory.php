<?php

use Faker\Generator as Faker;

$factory->define(App\Cliente::class, function (Faker $faker) {
    return [
        'dni'		=> $faker->unique()->numerify($string = '########'),
        'nombre'	=> $faker->name,
        'apellido'	=> $faker->lastName,
        'telefono'	=> $faker->unique()->numerify($string = '##########'),
        'direccion'	=> $faker->streetAddress,
        'email'		=> $faker->freeEmail,
    ];
});
