<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToOrdenesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ordenes', function (Blueprint $table) {
            $table->string('estado')->nullable()->after('accesorios');
            $table->string('prestador')->nullable()->after('estado');
            $table->string('clave')->nullable()->after('prestador');
            $table->string('diagnostico')->nullable()->after('precio');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ordenes', function (Blueprint $table) {
            $table->dropColumn('estado');
            $table->dropColumn('prestador');
            $table->dropColumn('clave');
            $table->dropColumn('diagnostico');
        });
    }
}
