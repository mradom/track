<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGarantiasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('garantias', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tipo');
            $table->date('fecha')->nullable();
            $table->string('lugar')->nullable(); 
            $table->timestamps();
        });

        Schema::table('ordenes', function (Blueprint $table) {
            $table->unsignedInteger('garantia_id')->after('accesorios');
            $table->foreign('garantia_id')->references('id')->on('garantias');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ordenes', function (Blueprint $table) {
            $table->dropColumn('garantia_id');
        });
        Schema::dropIfExists('garantias');
    }
}
