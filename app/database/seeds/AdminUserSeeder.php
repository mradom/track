<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Silber\Bouncer\BouncerFacade as Bouncer;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            "name" => "Omar Mrad",
            "email" => "mradom@codigitar.com",
            "password" => Hash::make('123456')
        ]);

        $user = User::create([
            "name" => "German Esposito",
            "email" => "gerleoesp@gmail.com",
            "password" => Hash::make('123456')
        ]);
    }
}
