<?php

use Illuminate\Database\Seeder;

class TiposTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tipos')->delete();
        
        \DB::table('tipos')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'Celular',
                'created_at' => '2019-05-02 05:39:13',
                'updated_at' => '2019-05-02 05:39:13',
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'Televisor',
                'created_at' => '2019-05-26 22:27:23',
                'updated_at' => '2019-05-26 22:27:23',
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'Tablet',
                'created_at' => '2019-08-03 15:17:37',
                'updated_at' => '2019-08-03 15:17:37',
            ),
        ));
        
        
    }
}