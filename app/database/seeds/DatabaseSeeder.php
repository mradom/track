<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // http://localhost/password/reset/a74f74c98c9cb160694fc94b37455b0d73f64e03e1616baada2bdf1848a06a8a
		//$this->call(AdminUserSeeder::class);
		$this->call(EstadosTableSeeder::class);
		$this->call(TiposTableSeeder::class);
		$this->call(MarcasTableSeeder::class);
    }
}
