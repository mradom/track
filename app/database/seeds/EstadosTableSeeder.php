<?php

use Illuminate\Database\Seeder;

class EstadosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('estados')->delete();
        
        \DB::table('estados')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'Ingreso',
                'orden' => 1,
                'created_at' => NULL,
                'updated_at' => '2019-05-12 19:25:09',
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'Diagnostico',
                'orden' => 2,
                'created_at' => '2019-05-08 22:42:51',
                'updated_at' => '2019-05-16 00:45:42',
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'Presupuesto',
                'orden' => 3,
                'created_at' => '2019-05-08 22:52:56',
                'updated_at' => '2019-05-16 00:45:42',
            ),
            3 => 
            array (
                'id' => 4,
                'nombre' => 'Presupuesto Aprobado',
                'orden' => 4,
                'created_at' => '2019-05-08 22:53:29',
                'updated_at' => '2019-05-16 00:44:15',
            ),
            4 => 
            array (
                'id' => 5,
                'nombre' => 'Pendiente por Material',
                'orden' => 5,
                'created_at' => '2019-05-08 22:54:16',
                'updated_at' => '2019-05-16 00:44:13',
            ),
            5 => 
            array (
                'id' => 6,
                'nombre' => 'Terminado Reparado',
                'orden' => 6,
                'created_at' => '2019-05-08 22:55:46',
                'updated_at' => '2019-05-16 00:44:11',
            ),
            6 => 
            array (
                'id' => 7,
                'nombre' => 'Terminado No Reparado',
                'orden' => 7,
                'created_at' => '2019-05-08 22:55:54',
                'updated_at' => '2019-05-16 00:44:08',
            ),
            7 => 
            array (
                'id' => 8,
                'nombre' => 'Entregado Reparado',
                'orden' => 8,
                'created_at' => '2019-05-08 22:56:11',
                'updated_at' => '2019-05-09 00:43:09',
            ),
            8 => 
            array (
                'id' => 9,
                'nombre' => 'Entregado No Reparado',
                'orden' => 9,
                'created_at' => '2019-05-08 22:56:20',
                'updated_at' => '2019-05-09 00:43:05',
            ),
        ));
        
        
    }
}