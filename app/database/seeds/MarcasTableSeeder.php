<?php

use Illuminate\Database\Seeder;

class MarcasTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('marcas')->delete();
        
        \DB::table('marcas')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'Samsung',
                'tipo_id' => 1,
                'created_at' => '2019-05-02 05:41:40',
                'updated_at' => '2019-05-02 05:41:40',
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'Motorola',
                'tipo_id' => 1,
                'created_at' => '2019-06-07 19:02:47',
                'updated_at' => '2019-08-03 15:05:28',
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'Nokia',
                'tipo_id' => 1,
                'created_at' => '2019-08-03 15:05:42',
                'updated_at' => '2019-08-03 15:05:42',
            ),
            3 => 
            array (
                'id' => 4,
                'nombre' => 'LG',
                'tipo_id' => 1,
                'created_at' => '2019-08-03 15:05:48',
                'updated_at' => '2019-08-03 15:05:48',
            ),
            4 => 
            array (
                'id' => 5,
                'nombre' => 'Huawei',
                'tipo_id' => 1,
                'created_at' => '2019-08-03 15:06:04',
                'updated_at' => '2019-08-03 15:06:04',
            ),
            5 => 
            array (
                'id' => 6,
                'nombre' => 'Alcatel',
                'tipo_id' => 1,
                'created_at' => '2019-08-03 15:06:17',
                'updated_at' => '2019-08-03 15:06:17',
            ),
            6 => 
            array (
                'id' => 7,
                'nombre' => 'Sony',
                'tipo_id' => 1,
                'created_at' => '2019-08-03 15:06:29',
                'updated_at' => '2019-08-03 15:06:29',
            ),
            7 => 
            array (
                'id' => 8,
                'nombre' => 'HTC',
                'tipo_id' => 1,
                'created_at' => '2019-08-03 15:06:38',
                'updated_at' => '2019-08-03 15:06:38',
            ),
            8 => 
            array (
                'id' => 9,
                'nombre' => 'BGH',
                'tipo_id' => 1,
                'created_at' => '2019-08-03 15:06:49',
                'updated_at' => '2019-08-03 15:06:49',
            ),
            9 => 
            array (
                'id' => 10,
                'nombre' => 'ZTE',
                'tipo_id' => 1,
                'created_at' => '2019-08-03 15:07:04',
                'updated_at' => '2019-08-03 15:07:04',
            ),
            10 => 
            array (
                'id' => 11,
                'nombre' => 'Apple',
                'tipo_id' => 1,
                'created_at' => '2019-08-03 15:07:10',
                'updated_at' => '2019-08-03 15:07:10',
            ),
            11 => 
            array (
                'id' => 12,
                'nombre' => 'Xiaomi',
                'tipo_id' => 1,
                'created_at' => '2019-08-03 15:07:22',
                'updated_at' => '2019-08-03 15:07:22',
            ),
            12 => 
            array (
                'id' => 13,
                'nombre' => 'Lenovo',
                'tipo_id' => 1,
                'created_at' => '2019-08-03 15:07:32',
                'updated_at' => '2019-08-03 15:07:32',
            ),
            13 => 
            array (
                'id' => 14,
                'nombre' => 'CAT',
                'tipo_id' => 1,
                'created_at' => '2019-08-03 15:07:55',
                'updated_at' => '2019-08-03 15:07:55',
            ),
            14 => 
            array (
                'id' => 15,
                'nombre' => 'TCL',
                'tipo_id' => 1,
                'created_at' => '2019-08-03 15:08:18',
                'updated_at' => '2019-08-03 15:08:18',
            ),
        ));
        
        
    }
}