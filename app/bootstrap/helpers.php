<?php

use Illuminate\Contracts\Auth\Factory as AuthFactory;

if (! function_exists('logged_user')) {
    /**
     * Gets current user (or attribute of user if $attribute defined).
     *
     * @param string $attribute
     *
     * @return \App\User|string|null Returns null if user is not logged in,
     */
    function logged_user($attribute = null)
    {
        if (! app(AuthFactory::class)->check()) {
            return null;
        }

        if (! empty($attribute)) {
            return app(AuthFactory::class)->user()->getAttribute($attribute);
        }

        return app(AuthFactory::class)->user();
    }
}
